<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\RankingController;
use App\Http\Controllers\LevelSoalController;
use App\Http\Controllers\SoalEssayController;
use App\Http\Controllers\TahapSoalController;
use App\Http\Controllers\EssayJawabanController;
use App\Http\Controllers\PengerjaanSoalController;
use App\Http\Controllers\OpsiPilihanGandaController;
use App\Http\Controllers\SoalPilihanGandaController;
use App\Http\Controllers\UserSelesaiUjianController;
use App\Http\Controllers\PilihanGandaJawabanController;
use App\Http\Controllers\UserController;
use App\Models\EssayJawaban;
use App\Models\LevelSoal;
use App\Models\PilihanGandaJawaban;
use App\Models\SoalEssay;
use App\Models\SoalPilihanGanda;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [PagesController::class, 'index']);
    Route::get('/jadwal', [PagesController::class, 'jadwal']);
    Route::post('/jadwal', [PagesController::class, 'updateTahap']);
    Route::resource('/tahap-soal', TahapSoalController::class);
    Route::resource('/soal-pilihan-ganda', SoalPilihanGandaController::class);
    Route::resource('/opsi-pilihan-ganda', OpsiPilihanGandaController::class);
    Route::post('/opsi-pilihan-ganda/deleteImage', [OpsiPilihanGandaController::class, 'deleteImage']);
    Route::post('/opsi-pilihan-ganda/deleteOpsi/{opsi_pilihan_ganda}', [OpsiPilihanGandaController::class, 'deleteOpsi']);
    Route::post('/opsi-pilihan-ganda/updateNewOpsi/{id}', [OpsiPilihanGandaController::class, 'addNewOpsi']);
    Route::resource('/soal-essay', SoalEssayController::class);
    Route::resource('/level-soal', LevelSoalController::class);
    Route::resource('/pengerjaan', PengerjaanSoalController::class);
    Route::resource('/pilihan-ganda', LevelSoalController::class);
    Route::resource('/ranking', RankingController::class);
    Route::resource('/jawaban-soal-essay', EssayJawabanController::class);
    Route::resource('/jawaban-soal-pg', PilihanGandaJawabanController::class);
    Route::post('/jawaban-essay/{soalid}', [EssayJawabanController::class, 'store']);

    Route::post('/selesai', [UserSelesaiUjianController::class, 'store']);

});

// Route::get('/admin-register', [PagesController::class, 'adminRegister']);
// Route::post('/admin-register', [PagesController::class, 'adminRegisterStore']);
Route::get('/peserta-register', [PagesController::class, 'pesertaRegister']);
Route::post('/peserta-register', [PagesController::class, 'pesertaRegisterStore']);

Route::get('/register', function(){
    return redirect('/login');
});

Route::get('/loaderio-9f9aaa3d7ccd19b97fe3ff0d7d6d827e.txt', function(){
    return view('loaderio-9f9aaa3d7ccd19b97fe3ff0d7d6d827e');
});

Route::get('/list-verifikasi', [UserController::class, 'listVerifikasi'])->middleware('admin');



// Import DATABASE USERS start
// Route::get('/import-tables', [ImportController::class, 'index']);
// Route::get('/user-verified', [ImportController::class, 'userVerified']);
// Route::post('/import-tables', [ImportController::class, 'store']);
// Route::post('/import-sekolah', [ImportController::class, 'importSekolah']);
// Route::delete('/reset-table-users', [ImportController::class, 'reset']);
// Route::get('/reset-all', function(){
    // SoalPilihanGanda::truncate();
    // SoalEssay::truncate();
    // EssayJawaban::truncate();
    // PilihanGandaJawaban::truncate();
    // LevelSoal::truncate();
    // User::where('sekolah_id', null)->delete();
// });
// Import DATABASE USERS end