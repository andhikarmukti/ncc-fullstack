<?php

namespace Database\Seeders;

use App\Models\SoalPilihanGanda;
use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SoalPilihanGandaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array_kunci = ['A', 'B', 'C', 'D', 'E'];
        $random_kunci = Arr::random($array_kunci);

        $array_level = [1, 2, 3, 4, 5];
        $random_level = Arr::random($array_level);

        for($i = 1; $i <=100; $i++){
            SoalPilihanGanda::create([
                'tahap_soal_id' => 1,
                'nomor_soal' => $i,
                'soal' => '<p>tes soal '. $i .'</p>',
                'kunci_jawaban' => $random_kunci,
                'level_soal_id' => $random_level
            ]);
        }
    }
}