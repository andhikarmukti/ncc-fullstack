<?php

namespace Database\Seeders;

use App\Models\OpsiPilihanGanda;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OpsiPilihanGandaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <=100; $i++){
            for($j = 1; $j <= 5; $j++){
                switch ($j) {
                case "1":
                    $huruf = 'A';
                    break;
                case "2":
                    $huruf = 'B';
                    break;
                case "3":
                    $huruf = 'C';
                    break;
                case "4":
                    $huruf = 'D';
                    break;
                case "5":
                    $huruf = 'E';
                    break;
                default:
                    $huruf = 'A';
                }
                OpsiPilihanGanda::create([
                    'soal_pilihan_ganda_id' => $i,
                    'huruf' => $huruf,
                    'isi_pilihan_jawaban' => 'ini adalah jawaban ' . $j
                ]);
            }
        }
    }
}