<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal_pilihan_gandas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tahap_soal_id');
            $table->string('nomor_soal');
            $table->longText('soal');
            $table->string('kunci_jawaban');
            $table->foreignId('level_soal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soal_pilihan_gandas');
    }
};
