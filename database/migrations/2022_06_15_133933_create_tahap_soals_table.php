<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahap_soals', function (Blueprint $table) {
            $table->id();
            $table->string('tahap_soal');
            $table->string('tanggal_ujian');
            $table->string('jam_mulai');
            $table->string('durasi_ujian');
            $table->enum('status_soal', ['selesai', 'belum dimulai', 'sedang dimulai'])->default('belum dimulai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahap_soals');
    }
};
