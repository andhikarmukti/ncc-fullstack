<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('essay_jawabans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('tahap_soal_id');
            $table->foreignId('soal_essay_id');
            $table->text('jawaban');
            $table->string('jawaban_file_upload')->nullable();
            $table->boolean('status_jawaban')->default(0);
            $table->foreignId('level_soal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('essay_jawabans');
    }
};