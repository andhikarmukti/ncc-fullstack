<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opsi_pilihan_gandas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('soal_pilihan_ganda_id');
            $table->string('huruf');
            $table->string('isi_pilihan_jawaban')->nullable();
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opsi_pilihan_gandas');
    }
};
