<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilihan_ganda_jawabans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('tahap_soal_id');
            $table->foreignId('soal_pilihan_ganda_id');
            $table->string('jawaban');
            $table->boolean('status_jawaban');
            $table->foreignId('level_soal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilihan_ganda_jawabans');
    }
};