<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoalPilihanGanda extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function tahap_soal()
    {
        return $this->hasOne(TahapSoal::class, 'id', 'tahap_soal_id');
    }

    public function level_soal()
    {
        return $this->hasOne(LevelSoal::class, 'id', 'level_soal_id');
    }

    public function opsi_pilihan_gandas()
    {
        return $this->hasMany(OpsiPilihanGanda::class, 'soal_pilihan_ganda_id', 'id');
    }

    public function pilihan_ganda_jawaban()
    {
        return $this->hasOne(PilihanGandaJawaban::class, 'soal_pilihan_ganda_id', 'id');
    }
}