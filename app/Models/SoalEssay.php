<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoalEssay extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function tahap_soal()
    {
        return $this->hasOne(TahapSoal::class, 'id', 'tahap_soal_id');
    }

    public function level_soal()
    {
        return $this->hasOne(LevelSoal::class, 'id', 'level_soal_id');
    }

    public function essay_jawaban()
    {
        return $this->hasOne(EssayJawaban::class, 'soal_essay_id', 'id');
    }
}
