<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PilihanGandaJawaban extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function soal_pilihan_ganda()
    {
        return $this->hasOne(SoalPilihanGanda::class, 'id', 'soal_pilihan_ganda_id');
    }

    public function level_soal()
    {
        return $this->hasOne(LevelSoal::class, 'id', 'level_soal_id');
    }

    public function tahap_soal()
    {
        return $this->hasOne(TahapSoal::class, 'id', 'tahap_soal_id');
    }
}