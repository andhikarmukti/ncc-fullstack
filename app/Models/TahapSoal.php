<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TahapSoal extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function soal_essays()
    {
        return $this->hasMany(SoalEssay::class)->orderByRaw('CAST(nomor_soal as UNSIGNED) ASC');
    }

    public function soal_pilihan_gandas()
    {
        return $this->hasMany(SoalPilihanGanda::class)->orderByRaw('CAST(nomor_soal as UNSIGNED) ASC');
    }

    public function rankings()
    {
        return $this->hasMany(Ranking::class);
    }

    public function pilihan_ganda_jawabans()
    {
        return $this->hasMany(PilihanGandaJawaban::class);
    }

    public function essay_jawabans()
    {
        return $this->hasMany(EssayJawaban::class);
    }
}