<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpsiPilihanGanda extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function soal_pilihan_ganda()
    {
        return $this->hasOne(SoalPilihanGanda::class, 'id', 'soal_pilihan_ganda_id');
    }
}
