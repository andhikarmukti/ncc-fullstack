<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LevelSoal extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function soal_pilihan_gandas()
    {
        return $this->hasMany(SoalPilihanGanda::class);
    }

    public function pilihan_ganda_jawabans()
    {
        return $this->hasMany(PilihanGandaJawaban::class);
    }

    public function soal_essays()
    {
        return $this->hasMany(SoalEssay::class);
    }

    public function essay_jawabans()
    {
        return $this->hasMany(EssayJawaban::class);
    }
}
