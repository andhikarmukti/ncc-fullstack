<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sekolah()
    {
        return $this->hasOne(Sekolah::class, 'id', 'sekolah_id');
    }

    public function pembayaran()
    {
        return $this->hasOne(Pembayaran::class, 'user_id', 'id');
    }

    public function tahap_soal()
    {
        return $this->hasOne(TahapSoal::class, 'id', 'tahap_soal_id');
    }

    public function pilihan_ganda_jawabans()
    {
        return $this->hasMany(PilihanGandaJawaban::class);
    }

    public function essay_jawabans()
    {
        return $this->hasMany(EssayJawaban::class);
    }

    public function rankings()
    {
        return $this->hasMany(Ranking::class);
    }

    public function log_errors()
    {
        return $this->hasMany(LogError::class);
    }

    public function user_selesai_ujians()
    {
        return $this->hasMany(UserSelesaiUjian::class);
    }
}