<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Ranking;
use App\Models\SoalEssay;
use App\Models\TahapSoal;
use Illuminate\Support\Str;
use App\Models\EssayJawaban;
use Illuminate\Http\Request;
use App\Models\PilihanGandaJawaban;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StoreEssayJawabanRequest;
use App\Http\Requests\UpdateEssayJawabanRequest;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class EssayJawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Periksa Jawaban Essay';
        $tahap_soals = TahapSoal::all();
        $users = User::orderBy('name', 'ASC')->get();
        $jawabans = [];

        if($request->ui && $request->tsi){
            $jawabans = EssayJawaban::where(['user_id' => $request->ui, 'tahap_soal_id' => $request->tsi])->get();
        }

        return view('jawaban_essay.index', compact(
            'title',
            'tahap_soals',
            'users',
            'jawabans'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEssayJawabanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SoalEssay $soalid)
    {
        $rules = [
            'jawaban_file_upload' => 'mimes:jpeg,png,jpg,gif,svg,pdf|max:50000|'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => 'validation error',
                'message' => $validator->errors()
            ], 400);
        }

        // Jika hanya edit jawaban text area nya saja
        if(!$request->jawaban_file_upload){
            $essay_jawaban = EssayJawaban::updateOrCreate([
                'user_id' => auth()->user()->id,
                'tahap_soal_id' => $soalid->tahap_soal_id,
                'soal_essay_id' => $soalid->id,
            ],
            [
                'user_id' => auth()->user()->id,
                'tahap_soal_id' => $soalid->tahap_soal_id,
                'soal_essay_id' => $soalid->id,
                'jawaban' => $request->jawaban,
                // 'jawaban_file_upload' => auth()->user()->id . $imageName,
                'level_soal_id' => $soalid->level_soal_id
            ]);

            return response()->json([
                'jawaban_file_upload' => $essay_jawaban->jawaban_file_upload,
                'jawaban' => $request->jawaban,
                'status' => 'success'
            ], 200);
        }

        $imageName = $request->jawaban_file_upload->getClientOriginalName();
        $imageExisting = EssayJawaban::where([
            'user_id' => auth()->user()->id,
            'tahap_soal_id' => $soalid->tahap_soal_id,
            'soal_essay_id' => $soalid->id,
        ])->first();
        if($imageExisting){
            if($imageExisting->jawaban_file_upload == $imageName){
                EssayJawaban::updateOrCreate([
                    'user_id' => auth()->user()->id,
                    'tahap_soal_id' => $soalid->tahap_soal_id,
                    'soal_essay_id' => $soalid->id,
                ],
                [
                    'user_id' => auth()->user()->id,
                    'tahap_soal_id' => $soalid->tahap_soal_id,
                    'soal_essay_id' => $soalid->id,
                    'jawaban' => $request->jawaban,
                    'jawaban_file_upload' => auth()->user()->id . $imageName,
                    'level_soal_id' => $soalid->level_soal_id
                ]);

                return response()->json([
                    'jawaban_file_upload' => auth()->user()->id . $imageName,
                    'status' => 'success'
                ], 200);
            }
        }
        if($imageExisting){
            File::delete(public_path('/images/jawaban-essay' . '/' . $imageExisting->jawaban_file_upload));
        }
        $request->jawaban_file_upload->move(public_path('images/jawaban-essay'), auth()->user()->id . $imageName);

        $essay_jawaban = EssayJawaban::updateOrCreate([
            'user_id' => auth()->user()->id,
            'tahap_soal_id' => $soalid->tahap_soal_id,
            'soal_essay_id' => $soalid->id,
        ],
        [
            'user_id' => auth()->user()->id,
            'tahap_soal_id' => $soalid->tahap_soal_id,
            'soal_essay_id' => $soalid->id,
            'jawaban' => $request->jawaban,
            'jawaban_file_upload' => auth()->user()->id . $imageName,
            'level_soal_id' => $soalid->level_soal_id
        ]);

        return response()->json([
            'essay_jawaban' => $essay_jawaban,
            'jawaban_file_upload' => auth()->user()->id . $imageName,
            'status' => 'success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EssayJawaban  $essayJawaban
     * @return \Illuminate\Http\Response
     */
    public function show(EssayJawaban $essayJawaban)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EssayJawaban  $essayJawaban
     * @return \Illuminate\Http\Response
     */
    public function edit(EssayJawaban $essayJawaban)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEssayJawabanRequest  $request
     * @param  \App\Models\EssayJawaban  $essayJawaban
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EssayJawaban $jawaban_soal_essay)
    {
        if($request->nilai >= 0){
            // $rules = [
            //     'nilai' => 'required|min:0|max:' . $request->bobotMax
            // ];
            // $validator = Validator::make($request->nilai, $rules);
            // if($validator->fails()){
            //     return response()->json([
            //         'status' => 'error',
            //         'message' => $validator->errors()
            //     ], 400);
            // }

            $jawaban = $jawaban_soal_essay->update([
                'nilai' => $request->nilai
            ]);
            return response()->json([
                'status' => 'success',
                'data' => $jawaban
            ], 200);
        }

        // update score
        $users = User::all();
            $scorePg = PilihanGandaJawaban::where([
                'user_id' => $jawaban_soal_essay->user_id,
                'tahap_soal_id' => $jawaban_soal_essay->tahap_soal_id,
                'status_jawaban' => 1
            ])
            ->get();
            $scoreEssay = EssayJawaban::where([
                'user_id' => $jawaban_soal_essay->user_id,
                'tahap_soal_id' => $jawaban_soal_essay->tahap_soal_id
            ])
            ->get();

            // Nilai PG
            $nilaiPg = [];
            foreach($scorePg as $spg){
                array_push($nilaiPg, $spg->level_soal->bobot_nilai);
            }
            $nilaiPg = array_sum($nilaiPg);

            // Nilai Essay
            $nilaiEssay = [];
            foreach($scoreEssay as $se){
                array_push($nilaiEssay, $se->nilai);
            }
            $nilaiEssay = array_sum($nilaiEssay);

            // Total nilai keduanya (pg dan essay)
            $totalNilai = $nilaiPg + $nilaiEssay;

            if(User::where('id', $jawaban_soal_essay->user_id)->first()->tahap_soal_id == $jawaban_soal_essay->tahap_soal_id){
                Ranking::updateOrCreate(
                    [
                        'user_id' => $jawaban_soal_essay->user_id,
                        'tahap_soal_id' => $jawaban_soal_essay->tahap_soal_id,
                    ],
                    [
                        'user_id' => $jawaban_soal_essay->user_id,
                        'tahap_soal_id' => $jawaban_soal_essay->tahap_soal_id,
                        'total_score' => $totalNilai,
                        'status' => $request->status
                    ]
                );
            }

        return response()->json([
            'status_jawaban' => $jawaban_soal_essay->status_jawaban == 1 ? 'benar' : 'salah'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EssayJawaban  $essayJawaban
     * @return \Illuminate\Http\Response
     */
    public function destroy(EssayJawaban $jawaban_soal_essay)
    {
        File::delete(public_path('images/jawaban-essay' . $jawaban_soal_essay->jawaban_file_upload));

        return $jawaban_soal_essay->update([
            'jawaban_file_upload' => ''
        ]);

        return response()->json([
            'data' => $jawaban_soal_essay
        ]);
    }
}