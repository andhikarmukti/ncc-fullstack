<?php

namespace App\Http\Controllers;

use App\Models\LogError;
use App\Models\LevelSoal;
use App\Models\TahapSoal;
use Illuminate\Http\Request;
use App\Models\OpsiPilihanGanda;
use App\Models\SoalPilihanGanda;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreSoalPilihanGandaRequest;
use App\Http\Requests\UpdateSoalPilihanGandaRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SoalPilihanGandaController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin', ['only' => ['store', 'update']]);
        $this->middleware('admin');
    }

    public function message()
    {
        return [
            'required' => 'Tidak boleh kosong!',
            'unique.nomor_soal' => 'Nomor soal sudah ada!'
        ];
    }

    public function rules()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $total_soal = SoalPilihanGanda::where('tahap_soal_id', $request->tahap_soal_id)->get();
            $last_create = SoalPilihanGanda::where('tahap_soal_id', $request->tahap_soal_id)->orderBy('id', 'DESC')->first();
            if ($last_create) {
                $ts = $total_soal->count();
                $ln = $last_create->nomor_soal;
            } else {
                $ts = 0;
                $ln = 0;
            }
            return response([$ts, $ln]);
        }

        $title = 'Pembuatan Soal Pilihan Ganda';
        $levels = LevelSoal::all();
        $tahaps = TahapSoal::all();
        $opsi_pilihan_gandas = OpsiPilihanGanda::all();

        $soal_pilihan_gandas = SoalPilihanGanda::with('tahap_soal')
            ->with('level_soal')
            ->with('opsi_pilihan_gandas')
            ->get();

        return view('soal_pilihan_ganda.index', compact(
            'title',
            'soal_pilihan_gandas',
            'levels',
            'tahaps',
            'opsi_pilihan_gandas'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSoalPilihanGandaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // dd(time() . '.' . $request->isian_jawaban_image[1]->extension());

        // pencegahan jika hanya ada 1 opsi jawaban
        if (!$request->length_loop) {
            ALert::error('Gagal! Opsi Jawaban tidak boleh jika hanya 1.');
            return back();
        }
        $rules = [
            'tahap_soal_id' => 'required',
            'nomor_soal' => ['required', 'min:1', Rule::unique('soal_pilihan_gandas')->where(function ($query) use ($request) {
                return $query->where('tahap_soal_id', $request->tahap_soal_id);
            })],
            'soal' => 'required',
            'huruf_jawaban.*' => 'required',
            'kunci_jawaban' => 'required',
            'level_soal_id' => 'required',
            'isian_jawaban_image*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:50000'
        ];
        $validator = Validator::make($request->all(), $rules, $this->message());
        if ($validator->fails()) {
            Alert::warning('Kesalahan Input!', 'Silahkan input ulang opsi jawaban');
            return redirect('/soal-pilihan-ganda#tahap_soal_id')
                ->withErrors($validator)
                ->withInput();
        }

        foreach ($request->huruf_jawaban as $hj) {
            if ($hj == $request->kunci_jawaban) {
                DB::beginTransaction();
                try {
                    $soal_pilihan_ganda = SoalPilihanGanda::create([
                        'tahap_soal_id' => $request->tahap_soal_id,
                        'nomor_soal' => $request->nomor_soal,
                        'soal' => $request->soal,
                        'kunci_jawaban' => $request->kunci_jawaban,
                        'level_soal_id' => $request->level_soal_id,
                    ]);

                    for ($i = 1; $i <= $request->length_loop; $i++) {
                        if (array_key_exists($i, $request->isian_jawaban)) {
                            if($request->isian_jawaban_image){
                                if (array_key_exists($i, $request->isian_jawaban_image)) {
                                    $imageName = auth()->user()->id . rand(1000, 99999) . '.' . $request->isian_jawaban_image[$i]->extension();
                                    $request->isian_jawaban_image[$i]->move(public_path('images/soal-pg'), $imageName);
                                }
                            }
                            OpsiPilihanGanda::create([
                                'soal_pilihan_ganda_id' => $soal_pilihan_ganda->id,
                                'huruf' => $request->huruf_jawaban[$i],
                                'isi_pilihan_jawaban' => $request->isian_jawaban[$i],
                                'image' => $request->isian_jawaban_image ? (array_key_exists($i, $request->isian_jawaban_image) ? $imageName : null) : null
                            ]);
                        }
                    }
                    DB::commit();

                    Alert::success('Berhasil!', 'Berhasil menambahkan soal');
                    return back();
                } catch (\Exception $e) {
                    DB::rollBack();
                    ALert::error('Gagal!', '500');
                    Log::error($e->getMessage());
                    return back();
                }
            }
        }
        Alert::error('Gagal!', 'Kunci jawaban tidak sesuai dengan opsi');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SoalPilihanGanda  $soalPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function show(SoalPilihanGanda $soalPilihanGanda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SoalPilihanGanda  $soalPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function edit(SoalPilihanGanda $soal_pilihan_ganda)
    {
        $title = 'Edit Soal Pilihan Ganda';
        $levels = LevelSoal::all();
        $tahaps = TahapSoal::all();
        $opsi_pilihan_gandas = OpsiPilihanGanda::where('soal_pilihan_ganda_id', $soal_pilihan_ganda->id)->get();
        // $abjad = [
        //     '0', 'A', 'B', 'C', 'D', 'E', 'F'
        // ];
        $selected = $soal_pilihan_ganda;


        return view('soal_pilihan_ganda.edit', compact(
            'title',
            'tahaps',
            'levels',
            'selected',
            'opsi_pilihan_gandas',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSoalPilihanGandaRequest  $request
     * @param  \App\Models\SoalPilihanGanda  $soalPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SoalPilihanGanda $soal_pilihan_ganda)
    {
        // dd($request->all());

        // pencegahan jika hanya ada 1 opsi jawaban
        if ($soal_pilihan_ganda->nomor_soal != $request->nomor_soal) {
            $rule_nomor_soal = ['required', 'min:1', Rule::unique('soal_pilihan_gandas')->where(function ($query) use ($request) {
                return $query->where('tahap_soal_id', $request->tahap_soal_id);
            })];
        } else {
            $rule_nomor_soal = ['required', 'min:1'];
        }
        $rules = [
            'tahap_soal_id' => 'required',
            'nomor_soal' => $rule_nomor_soal,
            'soal' => 'required',
            'kunci_jawaban' => 'required',
            'level_soal_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules, $this->message());
        if ($validator->fails()) {
            Alert::warning('Kesalahan Input!', 'Silahkan input ulang opsi jawaban');
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $opsi_hurufs = OpsiPilihanGanda::where('soal_pilihan_ganda_id', $soal_pilihan_ganda->id)->get();
        foreach($opsi_hurufs as $opsi_huruf){
            if ($opsi_huruf->huruf == $request->kunci_jawaban) {
                DB::beginTransaction();
                try {
                    $soal_pilihan_ganda->update([
                        'tahap_soal_id' => $request->tahap_soal_id,
                        'nomor_soal' => $request->nomor_soal,
                        'soal' => $request->soal,
                        'kunci_jawaban' => $request->kunci_jawaban,
                        'level_soal_id' => $request->level_soal_id,
                    ]);
                    DB::commit();
                    Alert::success('Berhasil!', 'Berhasil mengubah soal');
                    return back();
                } catch (\Exception $e) {
                    DB::rollBack();
                    ALert::error('Gagal!', '500');
                    Log::error($e->getMessage());
                    return back();
                }
            }
        }

        Alert::error('Gagal!', 'Kunci jawaban tidak sesuai dengan opsi');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SoalPilihanGanda  $soalPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoalPilihanGanda $soalPilihanGanda)
    {
        //
    }
}
