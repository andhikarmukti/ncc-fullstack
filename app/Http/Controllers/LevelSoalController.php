<?php

namespace App\Http\Controllers;

use App\Models\LevelSoal;
use App\Models\SoalPilihanGanda;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class LevelSoalController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $level = LevelSoal::find($request->level_id);

            return response($level);
        }

        $title = 'Level Soal';
        $levels = LevelSoal::all();

        return view('level_soal.index', compact(
            'title',
            'levels'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'level' => 'required',
            'nilai_benar' => 'required|numeric|min:0',
            'nilai_salah' => 'required|numeric'
        ], [
            'required' => 'Tidak boleh kosong!',
            'min' => 'Tidak boleh dibawah 0',
            'numeric' => 'Harus berupa angka'
        ]);

        LevelSoal::create($request->all());
        Alert::success('Berhasil!', 'Berhasil menambah data level soal.');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LevelSoal  $levelSoal
     * @return \Illuminate\Http\Response
     */
    public function show(LevelSoal $levelSoal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LevelSoal  $levelSoal
     * @return \Illuminate\Http\Response
     */
    public function edit(LevelSoal $levelSoal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LevelSoal  $levelSoal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LevelSoal $level_soal)
    {
        $rules = [
            'level' => 'required',
            'nilai_benar' => 'required|numeric|min:0',
            'nilai_salah' => 'required|numeric'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'error' => $validator->errors()
            ], 400);
        }

        $level_soal->update([
            'level' => $request->level,
            'nilai_benar' => $request->nilai_benar,
            'nilai_salah' => $request->nilai_salah
        ]);
        return response()->json([
            'status' => 'ok',
            'id' => $level_soal
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LevelSoal  $levelSoal
     * @return \Illuminate\Http\Response
     */
    public function destroy(LevelSoal $levelSoal)
    {
        //
    }
}