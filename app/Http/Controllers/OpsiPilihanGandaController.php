<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OpsiPilihanGanda;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreOpsiPilihanGandaRequest;
use App\Http\Requests\UpdateOpsiPilihanGandaRequest;
use Illuminate\Support\Facades\File;

class OpsiPilihanGandaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOpsiPilihanGandaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpsiPilihanGanda  $opsiPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function show(OpsiPilihanGanda $opsiPilihanGanda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OpsiPilihanGanda  $opsiPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function edit(OpsiPilihanGanda $opsiPilihanGanda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOpsiPilihanGandaRequest  $request
     * @param  \App\Models\OpsiPilihanGanda  $opsiPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpsiPilihanGanda $opsiPilihanGanda)
    {
        if(!$request->image_new){
            $opsiPilihanGanda->update([
                'huruf' => $request->huruf,
                'isi_pilihan_jawaban' => $request->isi_pilihan_jawaban
            ]);
            Alert::success('Berhasil!', 'Berhasil merubah data!');
            return back();
        }

        $imageName = time() .  '.' . $request->image_new->extension();
        $image_path = public_path('/images/soal-pg/' . $request->image_old);
        if(File::exists($image_path)){
            File::delete($image_path);
        }
        $request->image_new->move(public_path('images/soal-pg'), $imageName);
        $opsiPilihanGanda->update([
            'huruf' => $request->huruf,
            'isi_pilihan_jawaban' => $request->isi_pilihan_jawaban,
            'image' => $imageName
        ]);
        Alert::success('Berhasil!', 'Berhasil merubah data!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpsiPilihanGanda  $opsiPilihanGanda
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpsiPilihanGanda $opsiPilihanGanda)
    {
        //
    }

    public function deleteImage(Request $request)
    {
        $opsiPilihanGanda = OpsiPilihanGanda::where('id', $request->id)->first();
        $image_path = public_path('/images/soal-pg/' . $opsiPilihanGanda->image);
        try{
            File::delete($image_path);
            $opsiPilihanGanda->update([
                'image' => null
            ]);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function deleteOpsi(OpsiPilihanGanda $opsiPilihanGanda)
    {
        if($opsiPilihanGanda->where('soal_pilihan_ganda_id', $opsiPilihanGanda->soal_pilihan_ganda_id)->get()->count() <= 2){
            return response()->json([
                'message' => 'Opsi jawaban tidak bisa kurang dari 2 pilihan!'
            ], 200);
        }
        try{
            if($opsiPilihanGanda->image){
                $image_path = public_path('/images/soal-pg/' . $opsiPilihanGanda->image);
                File::delete($image_path);
            }
            $opsiPilihanGanda->delete();
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function addNewOpsi(Request $request, $id)
    {
        try{
            if($request->image){
                $imageName = time() .  '.' . $request->image->extension();
                $request->image->move(public_path('images/soal-pg'), $imageName);
            }else{
                $imageName = null;
            }
            OpsiPilihanGanda::create([
                'soal_pilihan_ganda_id' => $id,
                'huruf' => $request->huruf,
                'isi_pilihan_jawaban' => $request->isi_pilihan_jawaban,
                'image' => $imageName
            ]);
            Alert::success('Berhasil!', 'Berhasil menambah opsi jawaban baru!');
            return back();
        }catch(\Exception $e){
            Alert::error('Error!', $e->getMessage());
            return back();
        }
    }
}
