<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class ImportController extends Controller
{
    public function index()
    {
        $title = 'Import';
        $users = User::all();
        $sekolahs = Sekolah::all();
        return view('import_table.index', compact(
            'title',
            'users',
            'sekolahs'
        ));
    }

    public function store(Request $request)
    {
        foreach($request->users as $user){
            if($user['email'] != 'andhikarmukti@gmail.com'){
                User::create([
                    'name' => $user['full_name'],
                    'no_hp' => $user['no_hp'],
                    'email' => $user['email'],
                    'password' => $user['password'],
                    'role' => $user['role'],
                    'tahap_soal_id' => 1,
                    'tidak_lulus' => 0,
                    'sekolah_id' => $user['sekolah_id'],
                ]);
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $request->users
        ], 200);
    }

    public function importSekolah(Request $request)
    {
        Log::error($request->sekolahs);
        foreach ($request->sekolahs as $sekolah) {
            Sekolah::create([
                'id' => $sekolah['id'],
                'asal_sekolah' => $sekolah['asal_sekolah'],
                'asal_kota' => $sekolah['asal_kota'],
                'asal_provinsi' => $sekolah['asal_provinsi'],
                'guru_pendamping' => $sekolah['guru_pendamping'],
                'no_hp_guru_pendamping' => $sekolah['no_hp_guru_pendamping'],
            ]);
        }
        Sekolah::create([
            "id" => 131,
            "asal_sekolah" => "MA UNGGULAN AMANATUL UMMAH PROGRAM MADRASAH BERTARAF INTERNASIONAL",
            "asal_kota" => "MOJOKERTO",
            "asal_provinsi" => "JAWA TIMUR",
            "guru_pendamping" => "UST HADI",
            "no_hp_guru_pendamping" => "085266890043",
        ]);
    }

    public function reset()
    {
        User::truncate();
        Sekolah::truncate();
        User::create([
            "name" => "Andhika Raharja Mukti",
            "no_hp" => "089656911145",
            "email" => "andhikarmukti@gmail.com",
            "role" => "admin",
            "email_verified_at" => null,
            "password" => Hash::make('asdasdasd'),
        ]);
        Alert::success('Berhasil!', 'Berhasil reset data users');
        return back();
    }

    public function userVerified()
    {
        User::where('sekolah_id', null)->delete();
        User::create([
            "name" => "Andhika Raharja Mukti",
            "no_hp" => "089656911145",
            "email" => "andhikarmukti@gmail.com",
            "role" => "admin",
            "email_verified_at" => null,
            "password" => Hash::make('asdasdasd'),
        ]);
        return redirect('/list-verifikasi');
    }
}