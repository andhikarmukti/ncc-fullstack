<?php

namespace App\Http\Controllers;

use App\Models\EssayJawaban;
use Illuminate\Http\Request;
use App\Models\SoalPilihanGanda;
use App\Models\PilihanGandaJawaban;
use App\Models\SoalEssay;
use App\Models\TahapSoal;
use DateTime;
use Illuminate\Support\Facades\Date;
use RealRashid\SweetAlert\Facades\Alert;

class PengerjaanSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            if($request->typeSoal == 'essay'){
                $soal = SoalEssay::with('level_soal')->where(['nomor_soal' => ltrim($request->nomor, 0), 'tahap_soal_id' => $request->tahapSoal])->first();
                $jawaban = EssayJawaban::where([
                    'user_id' => auth()->user()->id,
                    'soal_essay_id' => $request->idSoal,
                    'tahap_soal_id' => $request->tahapSoal
                ])
                ->first();
                return response()->json([
                    'soal' => $soal,
                    'opsi' => $soal->opsi_pilihan_gandas,
                    'jawaban' => $jawaban,
                    'tombol_essay' => $jawaban ? 'edit' : 'submit',
                    'warna_tombol' => $jawaban ? 'warning' : 'primary'
                ]);
            }else{
                $soal = SoalPilihanGanda::with('level_soal')->where(['nomor_soal' => ltrim($request->nomor, 0), 'tahap_soal_id' => $request->tahapSoal])->first()->makeHidden('level_soal_id');
                $jawaban = PilihanGandaJawaban::where([
                    'user_id' => auth()->user()->id,
                    'soal_pilihan_ganda_id' => $request->idSoal,
                    'tahap_soal_id' => $request->tahapSoal
                ])
                ->first();

                return response()->json([
                    'soal' => $soal,
                    'opsi' => $soal->opsi_pilihan_gandas,
                    'jawaban' => $jawaban,
                    'tombol_essay' => $jawaban ? 'edit' : 'submit',
                    'warna_tombol' => $jawaban ? 'warning' : 'primary'
                ]);
            }
        }

        $user = auth()->user();
        $title = $user->tahap_soal->tahap_soal;

        $soal_pilihan_gandas = TahapSoal::find($user->tahap_soal_id)->soal_pilihan_gandas;
        $soal_essays = TahapSoal::find($user->tahap_soal_id)->soal_essays;

        $jawaban_pg = new PilihanGandaJawaban;
        $jawaban_essay = new EssayJawaban;

        $total_soal_pg = count($soal_pilihan_gandas);
        $total_soal_essay = count($soal_essays);

        $tahap = auth()->user()->tahap_soal;

        // Pengecekan bila jadwal belum dimulai
        $tanggal = $user->tahap_soal->tanggal_ujian;
        $jam = $user->tahap_soal->jam_mulai;
        $waktu_ujian = date($tanggal . ' ' . $jam);
        if(now() < $waktu_ujian){
            Alert::warning('Akses Dilarang!', $tahap->tahap_soal . ' belum dimulai');
            return back();
        }

        if(auth()->user()->tidak_lulus == 1){
            Alert::warning('Akses Dilarang!', 'Maaf, kamu sudah dinyatakan tidak lulus');
            return back();
        }

        if(auth()->user()->user_selesai_ujians->where('tahap_soal_id', $tahap->id)->first()){
            Alert::warning('Akses Dilarang!', 'Maaf, kamu sudah selesai melakukan ujian. Silahkan tunggu nilainya');
            return back();
        }

        return view('pengerjaan_soal.index', compact(
            'title',
            'soal_pilihan_gandas',
            'soal_essays',
            'jawaban_pg',
            'jawaban_essay',
            'total_soal_essay',
            'total_soal_pg',
            'tahap'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request->idsoal);
        if($request->ajax()){
            if(auth()->user()->tahap_soal->status_soal != 'sedang dimulai'){
                return response()->json([
                    'data' => 'error'
                ], 403);
            }
            $soal_pilihan_ganda = SoalPilihanGanda::find($request->idsoal);
            $level_soal_id = $soal_pilihan_ganda->level_soal_id;
            $kunci_jawaban = $soal_pilihan_ganda->kunci_jawaban;
            $tahap_soal_id = $soal_pilihan_ganda->tahap_soal_id;
            if($request->huruf == $kunci_jawaban){
                $status_jawaban = 1;
            }else{
                $status_jawaban = 0;
            }
            $jawaban = PilihanGandaJawaban::updateOrCreate(
            [
                'user_id' => auth()->user()->id,
                'tahap_soal_id' => $tahap_soal_id,
                'soal_pilihan_ganda_id' => $request->idsoal,
            ],
            [
                'user_id' => auth()->user()->id,
                'tahap_soal_id' => $tahap_soal_id,
                'soal_pilihan_ganda_id' => $request->idsoal,
                'jawaban' => $request->huruf,
                'level_soal_id' => $level_soal_id,
                'status_jawaban' => $status_jawaban
            ]);
            return response()->json([
                'status' => 'success',
                'data' => $jawaban
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}