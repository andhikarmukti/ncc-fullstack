<?php

namespace App\Http\Controllers;

use App\Models\UserSelesaiUjian;
use Illuminate\Http\Request;

class UserSelesaiUjianController extends Controller
{
    public function store(Request $request)
    {
        UserSelesaiUjian::create([
            'user_id' => auth()->user()->id,
            'tahap_soal_id' => $request->tahap_soal_id
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }
}
