<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
   public function listVerifikasi()
   {
        $title = 'List Verifikasi Users';
        $users = User::where('role', 'peserta')->get();
        // dd($users);

        return view('list-verifikasi-users.index', compact(
            'title',
            'users'
        ));
   }
}
