<?php

namespace App\Http\Controllers;

use App\Models\LevelSoal;
use App\Models\SoalEssay;
use App\Models\TahapSoal;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\OpsiPilihanGanda;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdateSoalEssayRequest;

class SoalEssayController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin', ['only' => ['store', 'update']]);
        $this->middleware('admin');
    }

    public function message()
    {
        return [
            'required' => 'Tidak boleh kosong!',
            'unique.nomor_soal' => 'Nomor soal sudah ada!'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $total_soal = SoalEssay::where('tahap_soal_id', $request->tahap_soal_id)->get();
            $last_number = SoalEssay::where('tahap_soal_id', $request->tahap_soal_id)->orderBy('nomor_soal', 'DESC')->first();
            if($last_number){
                $ts = $total_soal->count();
                $ln = $last_number->nomor_soal;
            }else{
                $ts = 0;
                $ln = 0;
            }
            return response([$ts, $ln]);
        }

        $levels = LevelSoal::all();
        $tahaps = TahapSoal::all();
        $title = 'Soal Essay';
        $soal_essays = SoalEssay::all();

        return view('soal_essay.index', compact(
            'title',
            'levels',
            'tahaps',
            'soal_essays'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSoalEssayRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'tahap_soal_id' => 'required',
            'nomor_soal' => ['required', 'min:1', Rule::unique('soal_essays')->where(function($query) use ($request){
                return $query->where('tahap_soal_id', $request->tahap_soal_id);
            })],
            'soal' => 'required',
            'level_soal_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules, $this->message());
        if($validator->fails()){
            Alert::warning('Kesalahan Input!', 'Silahkan input ulang opsi jawaban');
            return redirect('/soal-essay#tahap_soal_id')
            ->withErrors($validator)
            ->withInput();
        }

        DB::beginTransaction();
        try{
            $soal_pilihan_ganda = SoalEssay::create([
                'tahap_soal_id' => $request->tahap_soal_id,
                'nomor_soal' => $request->nomor_soal,
                'soal' => $request->soal,
                'level_soal_id' => $request->level_soal_id,
            ]);
            DB::commit();
            Alert::success('Berhasil!', 'Berhasil menambahkan soal');
            return back();
        }catch(\Exception $e){
            DB::rollBack();
            ALert::error('Gagal!', '500');
            Log::error($e->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SoalEssay  $soalEssay
     * @return \Illuminate\Http\Response
     */
    public function show(SoalEssay $soalEssay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SoalEssay  $soalEssay
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SoalEssay $soal_essay)
    {
        if($request->ajax()){
            $total_soal = SoalEssay::where('tahap_soal_id', $request->tahap_soal_id)->get();
            $last_number = SoalEssay::where('tahap_soal_id', $request->tahap_soal_id)->orderBy('nomor_soal', 'DESC')->first();
            if($last_number){
                $ts = $total_soal->count();
                $ln = $last_number->nomor_soal;
            }else{
                $ts = 0;
                $ln = 0;
            }
            return response([$ts, $ln]);
        }

        $levels = LevelSoal::all();
        $tahaps = TahapSoal::all();
        $title = 'Soal Essay';
        $soal_essay = $soal_essay;

        return view('soal_essay.edit', compact(
            'title',
            'levels',
            'tahaps',
            'soal_essay'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSoalEssayRequest  $request
     * @param  \App\Models\SoalEssay  $soalEssay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SoalEssay $soal_essay)
    {
        if($soal_essay->nomor_soal != $request->nomor_soal){
            $rule_nomor_soal = ['required', 'min:1', Rule::unique('soal_essays')->where(function($query) use ($request){
                return $query->where('tahap_soal_id', $request->tahap_soal_id);
            })];
        }else{
            $rule_nomor_soal = ['required', 'min:1'];
        }

        $rules = [
            'tahap_soal_id' => 'required',
            'nomor_soal' => $rule_nomor_soal,
            'soal' => 'required',
            'level_soal_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules, $this->message());
        if($validator->fails()){
            Alert::warning('Kesalahan Input!', 'Silahkan periksa kembali');
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        DB::beginTransaction();
        try{
            $soal_essay->update([
                'tahap_soal_id' => $request->tahap_soal_id,
                'nomor_soal' => $request->nomor_soal,
                'soal' => $request->soal,
                'level_soal_id' => $request->level_soal_id,
            ]);

            DB::commit();
            Alert::success('Berhasil!', 'Berhasil merubah data');
            return redirect('/soal-essay');
        }catch(\Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            Alert::error('Gagal!', '500!');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SoalEssay  $soalEssay
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoalEssay $soalEssay)
    {
        //
    }
}