<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\TahapSoal;
use App\Models\PilihanGandaJawaban;
use App\Http\Requests\StorePilihanGandaJawabanRequest;
use App\Http\Requests\UpdatePilihanGandaJawabanRequest;
use Illuminate\Http\Request;

class PilihanGandaJawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Jawaban Pilihan Ganda';
        $tahap_soals = TahapSoal::all();
        $users = User::orderBy('name', 'ASC')->get();
        $jawabans = [];

        if($request->ui && $request->tsi){
            $jawabans = PilihanGandaJawaban::where(['user_id' => $request->ui, 'tahap_soal_id' => $request->tsi])->get();
        }

        return view('jawaban_pg.index', compact(
            'title',
            'tahap_soals',
            'users',
            'jawabans'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePilihanGandaJawabanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePilihanGandaJawabanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PilihanGandaJawaban  $pilihanGandaJawaban
     * @return \Illuminate\Http\Response
     */
    public function show(PilihanGandaJawaban $jawaban_soal_pg)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PilihanGandaJawaban  $pilihanGandaJawaban
     * @return \Illuminate\Http\Response
     */
    public function edit(PilihanGandaJawaban $jawaban_soal_pg)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePilihanGandaJawabanRequest  $request
     * @param  \App\Models\PilihanGandaJawaban  $pilihanGandaJawaban
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePilihanGandaJawabanRequest $request, PilihanGandaJawaban $jawaban_soal_pg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PilihanGandaJawaban  $pilihanGandaJawaban
     * @return \Illuminate\Http\Response
     */
    public function destroy(PilihanGandaJawaban $jawaban_soal_pg)
    {
        //
    }
}