<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Ranking;
use App\Models\TahapSoal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public function index()
    {
        $title = 'Home';
        $total_score = Ranking::where(['user_id' => auth()->user()->id, 'tahap_soal_id' => auth()->user()->tahap_soal_id])->first();
        $user = new User;
        $tahap = new TahapSoal;

        return view('home', compact(
            'title',
            'total_score',
            'user',
            'tahap'
        ));
    }

    public function updateTahap(Request $request)
    {
        $tahap_soal = TahapSoal::find($request->id);
        if($request->ajax()){
            if($request->status == 'mulai'){
                $tahap_soal->status_soal = 'sedang dimulai';
            }else{
                $tahap_soal->status_soal = 'selesai';
            }
            $tahap_soal->save();
            return response()->json([
                'status' => 'berhasil',
                'status_soal' => $tahap_soal->status_soal
            ]);
        }
    }

    public function jadwal()
    {
        $title = 'Jadwal';
        $tahap = auth()->user()->tahap_soal;

        return view('jadwal.index', compact(
            'title',
            'tahap'
        ));
    }

    public function adminRegister()
    {
        $title = 'Admin Registrasi';
        return view('admin-register', compact(
            'title'
        ));
    }

    public function pesertaRegister()
    {
        $title = 'Peserta Registrasi';
        $users = User::where('tidak_lulus', 0)->get();
        return view('peserta-register', compact(
            'title',
            'users'
        ));
    }

    public function adminRegisterStore(Request $request)
    {
        $rules = [
            'name' => 'required',
            'no_hp' => 'required|numeric|min:8',
            'email' => 'required|unique:users|email',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            Alert::warning('Register Gagal!', 'Pastikan mengisi form dengan benar.');
            return back();
        }
        User::create([
            'name' => $request->name,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'admin'
        ]);
        Alert::success('Berhasil!', 'Berhasil menambah data!');
        return back();
    }

    public function pesertaRegisterStore(Request $request)
    {
        $rules = [
            'name' => 'required',
            'no_hp' => 'required|numeric|min:8',
            'email' => 'required|unique:users|email',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            Alert::warning('Register Gagal!', 'Pastikan mengisi form dengan benar.');
            return back();
        }
        User::create([
            'name' => $request->name,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'tahap_soal_id' => 3,
            'password' => Hash::make($request->password),
            'role' => 'peserta'
        ]);
        Alert::success('Berhasil!', 'Berhasil menambah data peserta!');
        return redirect('/login');
    }
}