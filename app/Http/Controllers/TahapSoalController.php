<?php

namespace App\Http\Controllers;

use App\Models\TahapSoal;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class TahapSoalController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function message()
    {
        return [
            'tahap_soal.required' => 'Kolom Tahap Soal tidak boleh kosong',
            'tanggal_ujian.required' => 'Kolom Tanggal Ujian tidak boleh kosong',
            'jam_mulai.required' => 'Kolom Jam Mulai tidak boleh kosong',
            'durasi_ujian.required' => 'Kolom Durasi Jam tidak boleh kosong',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Tahap Soal';
        $tahap_soals = TahapSoal::all();

        return view('tahap_soal.index', compact(
            'title',
            'tahap_soals'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'tahap_soal' => 'required',
            'tanggal_ujian' => 'required',
            'jam_mulai' => 'required',
            'durasi_ujian' => 'required',
        ], $this->message());

        if(TahapSoal::all()->count() < 4){
            TahapSoal::create($request->all());
        }else{
            Alert::warning('Data Limit!', 'Sudah ada 4 tahap, silahkan edit untuk merubah.');
            return back();
        }

        Alert::success('Berhasil!', 'Berhasil menambahkan tahap soal baru');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TahapSoal  $tahapSoal
     * @return \Illuminate\Http\Response
     */
    public function show(TahapSoal $tahapSoal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TahapSoal  $tahapSoal
     * @return \Illuminate\Http\Response
     */
    public function edit(TahapSoal $tahap_soal)
    {
        $title = "Edit";

        return view('tahap_soal.edit', compact(
            'title',
            'tahap_soal'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TahapSoal  $tahapSoal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TahapSoal $tahap_soal)
    {
        $validated = $request->validate([
            'tahap_soal' => 'required',
            'tanggal_ujian' => 'required',
            'jam_mulai' => 'required',
            'durasi_ujian' => 'required',
            'status_soal' => 'required',
        ], $this->message());

        $tahap_soal->update($request->all());

        Alert::toast('Berhasil update data', 'success');
        return redirect('/tahap-soal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TahapSoal  $tahapSoal
     * @return \Illuminate\Http\Response
     */
    public function destroy(TahapSoal $tahapSoal)
    {
        //
    }
}