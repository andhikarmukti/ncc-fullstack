<?php

namespace App\Http\Controllers;

use App\Models\Ranking;
use App\Http\Requests\StoreRankingRequest;
use App\Http\Requests\UpdateRankingRequest;
use App\Models\EssayJawaban;
use App\Models\PilihanGandaJawaban;
use App\Models\TahapSoal;
use App\Models\User;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Ranking';
        $tahap_soals = TahapSoal::all();
        $users = User::all();
        $rankings = [];
        $users = User::all();

        if($request->tsi){
            foreach($users as $user){
                $scoreBenarPg = PilihanGandaJawaban::where([
                    'user_id' => $user->id,
                    'tahap_soal_id' => $request->tsi,
                    'status_jawaban' => 1
                ])
                ->get();
                $scoreSalahPg = PilihanGandaJawaban::where([
                    'user_id' => $user->id,
                    'tahap_soal_id' => $request->tsi,
                    'status_jawaban' => 0
                ])
                ->get();
                $scoreEssay = EssayJawaban::where([
                    'user_id' => $user->id,
                    'tahap_soal_id' => $request->tsi
                ])
                ->get();

                // Nilai PG
                $nilaiSalah = [];
                foreach($scoreSalahPg as $sspg){
                    array_push($nilaiSalah, $sspg->level_soal->nilai_salah);
                }
                $nilaiBenar = [];
                foreach($scoreBenarPg as $sbpg){
                    array_push($nilaiBenar, $sbpg->level_soal->nilai_benar);
                }
                $nilaiBenar = array_sum($nilaiBenar);
                $nilaiSalah = array_sum($nilaiSalah);

                // Nilai Essay
                $nilaiEssay = [];
                foreach($scoreEssay as $se){
                    array_push($nilaiEssay, $se->nilai);
                }
                $nilaiEssay = array_sum($nilaiEssay);

                // Total nilai keduanya (pg dan essay)
                $totalNilai = ($nilaiBenar + $nilaiSalah) + $nilaiEssay;
                // dd(['nilai essay' => $nilaiEssay, 'total nilai' => $totalNilai, 'nilai benar' => $nilaiBenar, 'nilai salah' => $nilaiSalah]);

                if(User::where('id', $user->id)->first()->tahap_soal_id == $request->tsi){
                    Ranking::updateOrCreate(
                        [
                            'user_id' => $user->id,
                            'tahap_soal_id' => $request->tsi,
                        ],
                        [
                            'user_id' => $user->id,
                            'tahap_soal_id' => $request->tsi,
                            'total_score' => $totalNilai,
                            'status' => $request->status
                        ]
                    );
                }
            }
            $rankings = Ranking::orderBy('total_score', 'DESC')->where('tahap_soal_id', $request->tsi)->get();
        }

        return view('ranking.index', compact(
            'title',
            'tahap_soals',
            'rankings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRankingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRankingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ranking  $ranking
     * @return \Illuminate\Http\Response
     */
    public function show(Ranking $ranking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ranking  $ranking
     * @return \Illuminate\Http\Response
     */
    public function edit(Ranking $ranking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRankingRequest  $request
     * @param  \App\Models\Ranking  $ranking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ranking $ranking)
    {
        $ranking->update([
            'status' => $request->status
        ]);

        if($request->status == 1){
            User::find($ranking->user_id)->update([
                'tahap_soal_id' => $ranking->tahap_soal_id + 1,
                'tidak_lulus' => 0
            ]);
        }else{
            User::find($ranking->user_id)->update([
                'tidak_lulus' => 1,
                'tahap_soal_id' => $ranking->tahap_soal_id
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => $ranking
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ranking  $ranking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ranking $ranking)
    {
        //
    }
}