@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <div class="row my-4">
            <h1 class="text-center">Soal Essay</h1>
        </div>
        {{-- Table Soal Essay Start --}}
        <div class="card shadow-sm">
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="align-middle table table-hover table-stripped shadow-sm border border-top-0 table-radius"
                        id="table-soal-essay">
                        <thead class="text-bg-dark text-light">
                            <tr>
                                <th scope="col" class="col-table-small">No.</th>
                                <th scope="col">Soal</th>
                                <th scope="col" class="col-table-medium">Level Soal</th>
                                <th scope="col" class="col-table-medium">Tahap Soal</th>
                                <th scope="col" class="text-center col-table-medium">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($soal_essays as $soal_essay)
                                <tr>
                                    <td scope="row">{{ $soal_essay->nomor_soal }}</td>
                                    <td>
                                        <div class="card shadow-sm">
                                            <div class="card-body">
                                                {!! $soal_essay->soal !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $soal_essay->level_soal->level }}</td>
                                    <td>{{ $soal_essay->tahap_soal->tahap_soal }}</td>
                                    <td class="text-center">
                                        <a href="/soal-essay/{{ $soal_essay->id }}/edit"><span
                                                class="badge text-bg-warning">edit</span></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- Table Soal Essay END --}}

        {{-- Form Soal Essay START --}}
        <div class="card shadow-sm mt-4 border-0 p-4 col col-lg-8" id="form-soal">
            <div class="card-body">
                <form method="POST" action="/soal-essay">
                    @csrf
                    <div class="row">
                        <div class="col border border-3 bg-light rounded p-3">
                            <div class="row">
                                <div class="col-12 col-md-8">
                                    <div class="mb-3">
                                        <label for="tahap_soal_id" class="form-label">Tahap Soal</label>
                                        <select name="tahap_soal_id" id="tahap_soal_id"
                                            class="form-select @error('tahap_soal_id') is-invalid @enderror">
                                            <option value="" disabled {{ old('tahap_soal_id') ? '' : 'selected' }}>
                                                --
                                            </option>
                                            @foreach ($tahaps as $tahap)
                                                <option value="{{ old('tahap_soal_id') ?: $tahap->id }}"
                                                    {{ old('tahap_soal_id') == $tahap->id ? 'selected' : '' }}>
                                                    {{ $tahap->tahap_soal }}</option>
                                            @endforeach
                                        </select>
                                        @error('tahap_soal_id')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-8 col-md-4">
                                    <div class="mb-3 text-md-center">
                                        <label for="soal" class="form-label" id="nomor-soal">Nomor Soal</label>
                                        <br>
                                        <input type="number" class="form-control @error('nomor_soal') is-invalid @enderror"
                                            name="nomor_soal" id="#input-nomor-soal" value="{{ old('nomor_soal') }}">
                                        <div id="input-nomor-soal-div"></div>
                                        @error('nomor_soal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="soal" class="form-label">Soal</label>
                                <textarea name="soal" id="soal" class="ckeditor form-control @error('soal') is-invalid @enderror">{{ old('soal') }}</textarea>
                                @error('soal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="level_soal_id" class="form-label">Bobot Nilai</label>
                                    <select name="level_soal_id" id="level_soal_id" class="form-select">
                                        @foreach ($levels as $level)
                                            <option value="{{ $level->id }}"
                                                {{ old('level_soal_id') == $level->id ? 'selected' : '' }}>
                                                {{ $level->level }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill mt-md-3">Submit</button>
                </form>
            </div>
        </div>
        {{-- Form Soal Essay END --}}
    </div>

    <script>
        $(document).ready(function() {
            var table = $('#table-soal-essay').DataTable({
                responsive: true,
                buttons: ['print', 'excel', 'pdf', 'colvis'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [5, 10, 15, 25, 50, -1],
                    [5, 10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2, 3, 4]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 2, 3, 4]
                    }
                ]
            });

            table.buttons().container()
                .appendTo('#table-peserta_wrapper .col-md-6:eq(0)');
        });

        $('#tahap_soal_id').change(function() {
            let id = $(this).val();
            $.ajax({
                url: '/soal-essay',
                type: 'GET',
                data: {
                    'tahap_soal_id': id
                },
                success: function(result) {
                    let total_soal = result[0];
                    let last_number = result[1];
                    $('#info-nomor').empty();
                    $('#input-nomor-soal-div').after(
                        `
                        <small class=" text-center fst-italic float-end" id="info-nomor">Total Soal : ` + total_soal +
                        ` | Last Number : ` + last_number + `</small>
                    `)
                }
            })
        })
    </script>
@endsection
