@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <div class="col">
            <a href="/soal-essay" class="btn btn-primary rounded-pill">Kembali</a>
        </div>
        <div class="card shadow-sm mt-4 border-0 p-4 col col-lg-8" id="form-soal">
            <h1 class="text-center">Edit Soal Essay</h1>
            <div class="card-body">
                <form method="POST" action="/soal-essay/{{ $soal_essay->id }}">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col border border-3 bg-light rounded p-3">
                            <div class="row">
                                <div class="col-12 col-md-8">
                                    <div class="mb-3">
                                        <label for="tahap_soal_id" class="form-label">Tahap Soal</label>
                                        <select name="tahap_soal_id" id="tahap_soal_id"
                                            class="form-select @error('tahap_soal_id') is-invalid @enderror">
                                            <option value="" disabled {{ old('tahap_soal_id') ? '' : 'selected' }}>
                                                --
                                            </option>
                                            @foreach ($tahaps as $tahap)
                                                <option value="{{ old('tahap_soal_id') ?: $tahap->id }}"
                                                    {{ old('tahap_soal_id') == $tahap->id ? 'selected' : ($soal_essay->tahap_soal->id == $tahap->id ? 'selected' : '') }}>
                                                    {{ $tahap->tahap_soal }}</option>
                                            @endforeach
                                        </select>
                                        @error('tahap_soal_id')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-8 col-md-4">
                                    <div class="mb-3 text-md-center">
                                        <label for="soal" class="form-label" id="nomor-soal">Nomor Soal</label>
                                        <br>
                                        <input type="number" class="form-control @error('nomor_soal') is-invalid @enderror"
                                            name="nomor_soal" id="#input-nomor-soal"
                                            value="{{ old('nomor_soal') ?: $soal_essay->nomor_soal }}">
                                        <div id="input-nomor-soal-div"></div>
                                        @error('nomor_soal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="soal" class="form-label">Soal</label>
                                <textarea name="soal" id="soal" class="ckeditor form-control @error('soal') is-invalid @enderror">{{ old('soal') ?: $soal_essay->soal }}</textarea>
                                @error('soal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="level_soal_id" class="form-label">Bobot Nilai</label>
                                    <select name="level_soal_id" id="level_soal_id" class="form-select">
                                        @foreach ($levels as $level)
                                            <option value="{{ $level->id }}"
                                                {{ old('level_soal_id') == $level->id ? 'selected' : ($level->id == $soal_essay->level_soal->id ? 'selected' : '') }}>
                                                {{ $level->level }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill mt-md-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
