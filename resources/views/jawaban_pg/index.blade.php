@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <h1 class="text-center mb-5">Halaman Hasil Jawaban PG</h1>
        <form action="jawaban-soal-pg" method="GET">
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <div class="card p-2 shadow-sm">
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-6 col-md-6">
                                    <small>Tahap Soal</small>
                                    <select class="form-select" aria-label="Default select example" name="tsi">
                                        <option selected disabled>Pilih Tahap Soal</option>
                                        @foreach ($tahap_soals as $tahap_soal)
                                            <option value="{{ $tahap_soal->id }}"
                                                {{ request('tsi') == $tahap_soal->id ? 'selected' : '' }}>
                                                {{ $tahap_soal->tahap_soal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 col-md-6">
                                    <small>User</small>
                                    <select class="form-select" aria-label="Default select example" name="ui">
                                        <option selected disabled>Pilih User</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}"
                                                {{ request('ui') == $user->id ? 'selected' : '' }}>{{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-primary mt-2" type="submit">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        {{-- table hasil jawaban start --}}
        <div class="card mt-4 shadow-sm">
            <div class="card-body">
                <table id="tablePg" class="align-middle table table-hover table-bordered">
                    <thead class="align-middle text-bg-dark">
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Soal</th>
                            <th scope="col">Jawaban</th>
                            <th scope="col">Status Jawaban</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jawabans as $jawaban)
                            <tr>
                                <th class="col-table-small" scope="row">{{ $jawaban->soal_pilihan_ganda->nomor_soal }}
                                </th>
                                <td>
                                    <div class="card shadow-sm rounded">
                                        <div class="card-body">
                                            {!! $jawaban->soal_pilihan_ganda->soal !!}
                                        </div>
                                    </div>
                                </td>
                                <td class="text-{{ $jawaban->jawaban }}">{{ $jawaban->jawaban }}.
                                    {{ $jawaban->soal_pilihan_ganda->opsi_pilihan_gandas->where('huruf', $jawaban->jawaban)->first()->isi_pilihan_jawaban }}
                                </td>
                                <td
                                    class="col-table-medium fw-bold fst-italic text-{{ $jawaban->status_jawaban == 0 ? 'danger' : 'success' }}">
                                    {{ $jawaban->status_jawaban == 0 ? 'salah' : 'benar' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{-- table hasil jawaban end --}}
    </div>

    <script>
        $(document).ready(function() {
            $('#tablePg').DataTable({
                responsive: true,
                columnDefs: [{
                    className: "dt-center",
                    targets: [0, 1, 2, 3]
                }]
            });
        });
    </script>
@endsection
