@extends('layouts.main')

@section('content')
<div class="container py-4">
    <div class="col">
        <a href="/soal-pilihan-ganda" class="btn btn-primary rounded-pill">Kembali</a>
        <h1 class="text-center">Edit Soal Pilihan Ganda</h1>
    </div>
    <div class="card shadow mt-4 col" id="form-soal">
        <div class="card-body">
            <form method="POST" action="/soal-pilihan-ganda/{{ $selected->id }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6 border border-3 bg-light rounded p-3">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div class="mb-3">
                                    <label for="tahap_soal_id" class="form-label">Tahap Soal</label>
                                    <select name="tahap_soal_id" id="tahap_soal_id" class="form-select @error('tahap_soal_id') is-invalid @enderror">
                                        <option value="" disabled {{ old('tahap_soal_id') ? '' : 'selected' }}>
                                            --
                                        </option>
                                        @foreach ($tahaps as $tahap)
                                        <option value="{{ $tahap->id }}" {{ old('tahap_soal_id') == $tahap->id ? 'selected' : ($tahap->id == $selected->tahap_soal_id ? 'selected' : '') }}>
                                            {{ $tahap->tahap_soal }}</option>
                                        @endforeach
                                    </select>
                                    @error('tahap_soal_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-4">
                                <div class="mb-3 text-md-center">
                                    <label for="soal" class="form-label" id="nomor-soal">Nomor Soal</label>
                                    <br>
                                    <input type="number" class="form-control @error('nomor_soal') is-invalid @enderror" name="nomor_soal" id="#input-nomor-soal" value="{{ old('nomor_soal') ?: $selected->nomor_soal }}">
                                    <div id="input-nomor-soal-div"></div>
                                    @error('nomor_soal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="soal" class="form-label">Soal</label>
                            <textarea name="soal" id="soal" class="ckeditor form-control @error('soal') is-invalid @enderror">{{ old('soal') ?: $selected->soal }}</textarea>
                            @error('soal')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="kunci_jawaban" class="form-label">Kunci
                                        Jawaban</label>
                                    <input type="text" class="form-control @error('kunci_jawaban') is-invalid @enderror" name="kunci_jawaban" value="{{ old('kunci_jawban') ?: $selected->kunci_jawaban }}">
                                    @error('kunci_jawaban')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="level_soal_id" class="form-label">Bobot Nilai</label>
                                    <select name="level_soal_id" id="level_soal_id" class="form-select">
                                        @foreach ($levels as $level)
                                        <option value="{{ $level->id }}" {{ old('level_soal_id') == $level->id ? 'selected' : ($level->id == $selected->level_soal_id ? 'selected' : '') }}>
                                            {{ $level->level }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning rounded-pill mt-md-3">Edit</button>
            </form>
        </div>
        <div class="col-md-6 bg-light border border-3 rounded p-3">
            <div class="mb-3">
                <div class="card p-4 bg-light" id="opsiJawaban">
                    <div class="row col">
                        <div class="row mb-5">
                            <div class="col">
                                <h5 class="text-center">Opsi Jawaban</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-2">Huruf</div>
                        <div class="col-12 col-md-8">Isian Jawaban</div>
                    </div>
                    @foreach ($opsi_pilihan_gandas as $opsi_pilihan_ganda)
                    <form action="/opsi-pilihan-ganda/{{ $opsi_pilihan_ganda->id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mt-2 opsi-jawaban" id="opsi-jawaban">
                            <div class="col-3 col-md-2">
                                <input type="text" class="form-control @error('huruf') is-invalid @enderror" name="huruf" value="{{ old('huruf_jawaban') ?: $opsi_pilihan_ganda->huruf }}">
                                @error('huruf')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 col-md-10">
                                <input type="text" class="form-control" name="isi_pilihan_jawaban" value="{{ old('isian_jawaban') ?: $opsi_pilihan_ganda->isi_pilihan_jawaban }}">
                                <input value="tes" type="file" class="form-control @error('image_new') is-invalid @enderror" name="image_new">
                                @error('image_new')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                                @if($opsi_pilihan_ganda->image)
                                <a type="button"><img width="100" height="100" src="/images/soal-pg/{{ $opsi_pilihan_ganda->image }}" alt="soal-pg" class="mt-1 opsiImage" data-bs-toggle="modal" data-bs-target="#modalImage"></a>
                                <button type="button" class="badge bg-danger deleteImage d-block mt-1 border-0" data-id={{ $opsi_pilihan_ganda->id }}>delete image</button>
                                @endif
                            </div>
                            <input type="hidden" name="image_old" value="{{ $opsi_pilihan_ganda->image }}">
                            <div class="row">
                                <div class="col-8"></div>
                                <div class="col-4 mt-1 gap-1">
                                    <button type="submit" class="btn btn-warning btn-sm">update</button>
                                    <button type="button" data-opsiid={{ $opsi_pilihan_ganda->id }} class="btn btn-danger btn-sm remove">delete opsi</button>
                                </div>
                            </div>
                            <hr class="mt-2">
                        </div>
                    </form>
                    @endforeach
                    <div id="tempat-opsi-jawaban"></div>
                    <div class="row mt-4">
                        <div class="col">
                            <button type="button" class="badge bg-primary border-0 float-end text-decoration-none" id="button-tambah-opsi">tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

{{-- modal image start --}}
<div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="modal-images">
                    <img src="" alt="" class="images-modal" width="100%"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- modal image end --}}

<script>
    $('.opsiImage').click(function(){
        $('.images-modal').attr('src', $(this).attr('src'));
    })

    // $('.opsiUpdate').click(function() {
    //     const opsiUpdateId = $(this).data('opsiid');
    //     const opsiUpdateHuruf = $(this).data('opsihuruf');
    //     const opsiUpdateIsiPilihanJawaban = $(this).data('opsiisipilihanjawaban');
    //     const opsiUpdateOldImage = $(this).data('opsiimage');
    //     const opsiUpdateFileImage = $(this).parent().parent().parent().find("input[type=file]").prop('files')[0];
    //     console.log(opsiUpdateFileImage);

    //     $.ajax({
    //         url: '/opsi-pilihan-ganda/' + opsiUpdateId,
    //         method: 'POST',
    //         contentType: false,
    //         processData: false,
    //         data: {
    //             '_token': "{{ csrf_token() }}",
    //             method : 'PUT',
    //             huruf: opsiUpdateHuruf,
    //             isi_pilihan_jawaban: opsiUpdateIsiPilihanJawaban,
    //             oldImage: opsiUpdateOldImage,
    //             newImage: opsiUpdateFileImage
    //         }
    //         , success: function(res) {
    //             console.log(res);
    //         }
    //     })
    // })

    $('.deleteImage').click(function() {
        const id = $(this).data('id');
        Swal.fire({
            title: 'Are you sure?'
            , text: "You won't be able to revert this!"
            , icon: 'warning'
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/opsi-pilihan-ganda/deleteImage',
                    method : 'POST',
                    data : {
                        id : id,
                        '_token' : "{{ csrf_token() }}"
                    },
                    success : function(res){
                        console.log(res);
                        if(!res){
                            Swal.fire(
                                'Deleted!'
                                , 'Your file has been deleted.'
                                , 'success'
                            ).then(() => {
                                location.reload();
                            })
                        }else{
                            Swal.fire(
                                'Error!'
                                , res
                                , 'error'
                            )

                        }
                    }
                });

            }
        })
        console.log(id);
    });

    function ckeditor() {
        $('.ckeditor').ckEditor();
    }

    function OpsiJawaban() {
        let length = $('#opsi-jawaban').data('length');
        for (var i = 1; i <= length; i++) {
            $('#opsi-jawaban').attr('id', 'opsi-jawaban[' + i + ']');
            $('#huruf-jawaban').attr('id', 'huruf-jawaban[' + i + ']').attr('name', 'huruf_jawaban[' + i + ']');
            $('#isian-jawaban').attr('id', 'isian-jawaban[' + i + ']').attr('name', 'isian_jawaban[' + i + ']');
            $('#isian_jawaban_image_old').attr('id', 'isian_jawaban_image_old[' + i + ']').attr('name', 'isian_jawaban_image_old[' + i + ']');
            $('input[id=length_loop_first]').val(i);
        }

        return i - 1;
    }

    var i = OpsiJawaban();
    $(document).ready(function() {
        $('#button-tambah-opsi').click(function() {
            $(this).remove();
            i = i + 1;
            $('input[id=length_loop_first]').val(i);
            if (i >= 10) {
                Swal.fire({
                    icon: 'error'
                    , title: 'Oops...'
                    , text: 'Terlalu banyak hapus dan tambah opsi jawaban'
                    , footer: '<p>Sebaiknya refresh page dan ulangi lagi. Terima kasih</p>'
                    , timer: 2000
                })
                // alert('hapus hapus mulu buset ya!');
            } else {
                $('#tempat-opsi-jawaban').before(`
                <h3 class="text-center">Tambah Opsi Baru</h3>
                <form action="/opsi-pilihan-ganda/updateNewOpsi/{{ $opsi_pilihan_ganda->soal_pilihan_ganda_id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row mt-2 opsi-jawaban" id="opsi-jawaban">
                            <div class="col-3 col-md-2">
                                <input type="text" class="form-control" name="huruf">
                            </div>
                            <div class="col-12 col-md-10">
                                <input type="text" class="form-control" name="isi_pilihan_jawaban">
                                <input value="tes" type="file" class="form-control @error('image') is-invalid @enderror" name="image">
                                @error('image')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-11"></div>
                                <div class="col-1 mt-1 gap-1">
                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                </div>
                            </div>
                            <hr class="mt-2">
                        </div>
                    </form>
                `)
            }
        });

        $('body').delegate('.remove', 'click', function() {
            const opsiId = $(this).data('opsiid');
            Swal.fire({
                title: 'Are you sure?'
                , text: "Kamu yakin akan menghapus opsi tersebut?"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(opsiId);
                    $.ajax({
                        url : '/opsi-pilihan-ganda/deleteOpsi/' + opsiId,
                        method : 'POST',
                        data : {
                            id : opsiId,
                            '_token' : "{{ csrf_token() }}"
                        },
                        success : function(res){
                            console.log(res);
                            if(!res){
                                Swal.fire(
                                    'Deleted!'
                                    , 'Your file has been deleted.'
                                    , 'success'
                                ).then(() => {
                                    location.reload();
                                })
                            }else{
                                Swal.fire(
                                    'Error!'
                                    , res.message
                                    , 'error'
                                )

                            }
                        }
                    });
                }
            })
            // $(this).parent().closest('div div .opsi-jawaban').remove();
            // i -= 1;
            // $('input[id=length_loop_first]').val(i);
        })


        $('#tahap_soal_id').change(function() {
            let id = $(this).val();
            $.ajax({
                url: '/soal-pilihan-ganda'
                , type: 'GET'
                , data: {
                    'tahap_soal_id': id
                }
                , success: function(result) {
                    console.log(result);
                    let total_soal = result[0];
                    let last_number = result[1];
                    console.log(last_number);
                    $('#info-nomor').empty();
                    $('#input-nomor-soal-div').after(
                        `
                        <small class=" text-center fst-italic float-end" id="info-nomor">Total Soal : ` + total_soal +
                        ` <br> Last Number : ` + last_number + `</small>
                    `)
                }
            })
        })
    })

</script>
@endsection
