@extends('layouts.main')

@section('content')
<div class="container py-4">
    {{-- Table soal pilihan ganda START --}}
    <div class="row my-4">
        <h1 class="text-center">Soal Pilihan Ganda</h1>
    </div>
    <div class="card shadow-sm">
        <div class="card-body p-4">
            <div class="table-responsive">
                <table class="table table-hover table-stripped shadow-sm border border-top-0 table-radius" id="table-soal-pilihan-ganda">
                    <thead class="bg-warning text-dark">
                        <tr class="text-center">
                            <th scope="col" class="col-table-small">No</th>
                            <th scope="col" class="col-table-large">Soal</th>
                            <th scope="col" class="col-table-small">Jawaban</th>
                            <th scope="col">Bobot Nilai</th>
                            <th scope="col">Tahap Soal</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($soal_pilihan_gandas as $soal_pilihan_ganda)
                        <tr>
                            <td>{{ $soal_pilihan_ganda->nomor_soal }}</td>
                            <td>
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        {!! $soal_pilihan_ganda->soal !!}
                                    </div>
                                    @foreach ($soal_pilihan_ganda->opsi_pilihan_gandas as $opsi)
                                    <div class="row ms-2">
                                        @if($opsi->isi_pilihan_jawaban == null)
                                        <div class="d-flex mb-2">
                                            <p class="me-1">{{ $opsi->huruf }}</p>
                                            @if($opsi->image)
                                            <a type="button"><img width="100" height="100" src="{{ '/images/soal-pg/' . $opsi->image }}" alt="" data-bs-toggle="modal" data-bs-target="#modalImage" class="opsiImage"></a>
                                            @endif
                                        </div>
                                        @else
                                        <p>{{ $opsi->huruf }}. {{ $opsi->isi_pilihan_jawaban }}</p>
                                        @if($opsi->image)
                                        <a type="button"><img width="100" height="100" src="{{ '/images/soal-pg/' . $opsi->image }}" alt="" data-bs-toggle="modal" data-bs-target="#modalImage" class="opsiImage"></a>
                                        @endif
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                            <td class="text-center">{{ $soal_pilihan_ganda->kunci_jawaban }}</td>
                            <td class="text-center">{{ $soal_pilihan_ganda->level_soal->level }} <br> [
                                <span class="text-success fw-bold">{{ $soal_pilihan_ganda->level_soal->nilai_benar
                                    }}</span> | <span class="text-danger fw-bold">{{
                                    $soal_pilihan_ganda->level_soal->nilai_salah }}</span> ]
                            </td>
                            <td class="text-center">{{ $soal_pilihan_ganda->tahap_soal->tahap_soal }}</td>
                            <td class="text-center">
                                <a href="/soal-pilihan-ganda/{{ $soal_pilihan_ganda->id }}/edit"><span class="badge bg-warning text-dark">edit</span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- Table soal pilihan ganda END --}}

    {{-- Button Add Tambah Soal START --}}
    <div class="col">
        <button class="btn bg-primary rouded-pill text-light mt-4 {{ $errors->all() ? 'd-none' : '' }}" id="button-tambah-soal">(+) Tambah Soal</button>
    </div>
    {{-- Button Add Tambah Soal START --}}

    {{-- Form Tambah Soal START --}}
    <div class="card shadow mt-4 col {{ $errors->all() ? '' : 'd-none' }}" id="form-soal">
        <div class="card-body">
            <form method="POST" action="/soal-pilihan-ganda" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6 border border-3 bg-light rounded p-3">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div class="mb-3">
                                    <label for="tahap_soal_id" class="form-label">Tahap Soal</label>
                                    <select name="tahap_soal_id" id="tahap_soal_id" class="form-select @error('tahap_soal_id') is-invalid @enderror">
                                        <option value="" disabled {{ old('tahap_soal_id') ? '' : 'selected' }}>
                                            --
                                        </option>
                                        @foreach ($tahaps as $tahap)
                                        <option value="{{ old('tahap_soal_id') ?: $tahap->id }}" {{
                                            old('tahap_soal_id')==$tahap->id ? 'selected' : '' }}>
                                            {{ $tahap->tahap_soal }}</option>
                                        @endforeach
                                    </select>
                                    @error('tahap_soal_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-4">
                                <div class="mb-3 text-md-center">
                                    <label for="soal" class="form-label" id="nomor-soal">Nomor Soal</label>
                                    <br>
                                    <input type="number" class="form-control @error('nomor_soal') is-invalid @enderror" name="nomor_soal" id="#input-nomor-soal" value="{{ old('nomor_soal') }}">
                                    <div id="input-nomor-soal-div"></div>
                                    @error('nomor_soal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="soal" class="form-label">Soal</label>
                            <textarea name="soal" id="soal" class="ckeditor form-control @error('soal') is-invalid @enderror">{{ old('soal') }}</textarea>
                            @error('soal')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 bg-light border border-3 rounded p-3">
                        <div class="mb-3">
                            <div class="card p-4 bg-light" id="opsiJawaban">
                                <div class="row col">
                                    <div class="row mb-5">
                                        <div class="col">
                                            <h5 class="text-center">Opsi Jawaban</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center mb-2" id="opsi-jawaban-1">
                                    <div class="col-3 col-md-2">
                                        <label for="huruf_jawaban_1" class="form-label">Huruf</label>
                                        <input type="text" class="form-control @error('huruf_jawaban.*') is-invalid @enderror" name="huruf_jawaban[1]" value="A">
                                        @error('huruf_jawaban.*')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-9 col-md-9">
                                        <label for="isian_jawaban*" class="form-label">Isi Jawaban</label>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control @error('isian_jawaban.*') is-invalid @enderror" name="isian_jawaban[1]">
                                                @error('isian_jawaban.*')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <input type="file" class="form-control @error('isian_jawaban_image.*') is-invalid @enderror" name="isian_jawaban_image[1]">
                                                @error('isian_jawaban_image.*')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-2 mb-2">
                                </div>
                                <div id="tempat-opsi-jawaban"></div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <button type="button" class="badge bg-primary border-0 float-end" id="button-tambah-opsi">tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="kunci_jawaban" class="form-label">Kunci Jawaban</label>
                                    <input type="text" class="form-control @error('kunci_jawaban') is-invalid @enderror" name="kunci_jawaban" value="{{ old('kunci_jawaban') }}">
                                    @error('kunci_jawaban')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="level_soal_id" class="form-label">Bobot Nilai</label>
                                    <select name="level_soal_id" id="level_soal_id" class="form-select">
                                        @foreach ($levels as $level)
                                        <option value="{{ $level->id }}" {{ old('level_soal_id')==$level->id ?
                                            'selected' : '' }}>
                                            {{ $level->level }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary rounded-pill mt-3">Submit</button>
            </form>
        </div>
    </div>
    {{-- Form Tambah Soal END --}}
</div>

{{-- modal image start --}}
<div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="modal-images">
                    <img src="" alt="" class="images-modal" width="100%"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- modal image end --}}

<script type="text/javascript">
    $('.opsiImage').click(function(){
        $('.images-modal').attr('src', $(this).attr('src'));
    })

    $(document).ready(function() {
        var table = $('#table-soal-pilihan-ganda').DataTable({
            responsive: true
            , buttons: ['print', 'excel', 'pdf', 'colvis']
            , dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-5'i><'col-md-7'p>>"
            , lengthMenu: [
                [5, 10, 15, 25, 50, -1]
                , [5, 10, 15, 25, 50, "All"]
            ]
            , columnDefs: [{
                    className: "dt-head-center"
                    , targets: [0, 1, 2, 3, 4, 5]
                }
                , {
                    className: "dt-body-center"
                    , targets: [0, 2, 3, 4, 5]
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table-peserta_wrapper .col-md-6:eq(0)');
    });

</script>

<script>
    let i = 1;
    $('#button-tambah-opsi').click(function() {
        i += 1;
        if (i >= 20) {
            Swal.fire({
                icon: 'error'
                , title: 'Oops...'
                , text: 'Terlalu banyak hapus dan tambah opsi jawaban'
                , footer: '<p>Sebaiknya refresh page dan ulangi lagi. Terima kasih</p>'
                , timer: 2000
            })
            // alert('hapus hapus mulu buset ya!');
        } else {
            $('#tempat-opsi-jawaban').before(`
                <div class="row align-items-center opsi-jawaban" id="opsi-jawaban-${i}">
                    <div class="col-3 col-md-2">
                        <input type="text" class="form-control @error('huruf_jawaban.*') is-invalid @enderror" name="huruf_jawaban[${i}]">
                        @error('huruf_jawaban.*')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-9 col-md-9">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" class="form-control @error('isian_jawaban.*') is-invalid @enderror" name="isian_jawaban[${i}]">
                                @error('isian_jawaban.*')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <input type="file" class="form-control @error('isian_jawaban_image.*') is-invalid @enderror" name="isian_jawaban_image[${i}]">
                                @error('isian_jawaban_image.*')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="cancel badge bg-danger border-0 rounded text-center">X</button>
                    </div>
                    <hr class="mt-2 mb-2">
                    <input type="hidden" name="length_loop" value=${i}>
                </div>
            `)
        }
    });

    $('body').delegate('.cancel', 'click', function() {
        $(this).parent().closest('div div .opsi-jawaban').remove();
    })

    $('#tahap_soal_id').change(function() {
        let id = $(this).val();
        $.ajax({
            url: '/soal-pilihan-ganda'
            , type: 'GET'
            , data: {
                'tahap_soal_id': id
            }
            , success: function(result) {
                console.log(result);
                let total_soal = result[0];
                let last_create = result[1];
                $('#info-nomor').empty();
                $('#input-nomor-soal-div').after(
                    `
                        <small class=" text-center fst-italic float-end" id="info-nomor">Total Soal : ` + total_soal +
                    ` | Last Create : ` + last_create + `</small>
                    `)
            }
        })
    })

    $('#button-tambah-soal').click(function() {
        $(this).addClass('d-none');
        $('#form-soal').toggleClass('d-none');
    });

</script>
@endsection
