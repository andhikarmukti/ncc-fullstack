@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <div class="card">
            <div class="card-body">
                <h1 class="text-center mb-5">Level Soal</h1>
                <div class="row">
                    <div class="col-12 col-lg-4 mb-5 mb-md-0">
                        <div class="card p-3 shadow-sm">
                            <form method="POST" action="/level-soal">
                                @csrf
                                <div class="mb-3">
                                    <label for="level" class="form-label">Level</label>
                                    <input type="text" class="form-control @error('level') is-invalid @enderror"
                                        id="level" name="level" value="{{ old('level') }}">
                                    @error('level')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="nilai_benar" class="form-label">Nilai Benar</label>
                                    <input type="number" class="form-control @error('nilai_benar') is-invalid @enderror"
                                        id="nilai_benar" name="nilai_benar" value="{{ old('nilai_benar') }}">
                                    @error('nilai_benar')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="nilai_salah" class="form-label">Nilai Salah</label>
                                    <input type="number" class="form-control @error('nilai_salah') is-invalid @enderror"
                                        id="nilai_salah" name="nilai_salah" value="{{ old('nilai_salah') }}">
                                    @error('nilai_salah')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-radius shadow-sm text-center">
                                <thead class="bg-dark text-light">
                                    <tr>
                                        <th scope="col">Level</th>
                                        <th scope="col">Nilai Benar</th>
                                        <th scope="col">Nilai Salah</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($levels as $level)
                                        <tr id="lid{{ $level->id }}">
                                            <td scope="row" id="row-level">{{ $level->level }}</td>
                                            <td id="row-nilai-benar" class="text-success fw-bold">{{ $level->nilai_benar }}</td>
                                            <td id="row-nilai-salah" class="text-danger fw-bold">{{ $level->nilai_salah }}</td>
                                            <td>
                                                <a href="#"><span class="badge text-bg-warning button-edit"
                                                        data-bs-toggle="modal" data-bs-target="#modal-edit"
                                                        data-id="{{ $level->id }}">edit</span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="fade modal fadein" id="modal-edit" tabindex="-1" aria-labelledby="modal-edit" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Level</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/level-soal" id="form-edit">
                        <div class="mb-3">
                            <label for="modal-level" class="form-label">Level</label>
                            <input type="text" class="form-control" id="modal-level" name="level">
                            <small id="level-error"></small>
                        </div>
                        <div class="mb-3">
                            <label for="modal-nilai-benar" class="form-label">Nilai Benar</label>
                            <input type="number" class="form-control" id="modal-nilai-benar" name="nilai_benar">
                            <small id="nilai-benar-error"></small>
                        </div>
                        <div class="mb-3">
                            <label for="modal-nilai-salah" class="form-label">Nilai Salah</label>
                            <input type="number" class="form-control" id="modal-nilai-salah" name="nilai_benar">
                            <small id="nilai-salah-error"></small>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn text-bg-warning" id="button-submit-edit">Edit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#modal-edit').on('hidden.bs.modal', function() {
                $('#level-error').removeClass('invalid-feedback');
                $('#modal-level').removeClass('is-invalid');
                $('#nilai-benar-error').removeClass('invalid-feedback');
                $('#nilai-salah-error').removeClass('invalid-feedback');
                $('#modal-nilai-benar').removeClass('is-invalid');
                $('#modal-nilai-salah').removeClass('is-invalid');
                $('#level-error').addClass('d-none');
                $('#nilai-benar-error').addClass('d-none');
                $('#nilai-salah-error').addClass('d-none');
            })
        });


        var id;
        $('.button-edit').click(function() {
            id = $(this).data('id');
            $.ajax({
                url: '/level-soal',
                type: 'GET',
                data: {
                    'level_id': id
                },
                success: function(result) {
                    $('#form-edit').attr('action', '/level-soal/' + id)
                    $('#modal-level').val(result['level']);
                    $('#modal-nilai-benar').val(result['nilai_benar']);
                    $('#modal-nilai-salah').val(result['nilai_salah']);
                }
            })
        });

        $('#button-submit-edit').click(function() {
            let token = $('input[name=_token]').val();
            let level = $('#modal-level').val();
            let nilai_benar = $('#modal-nilai-benar').val();
            let nilai_salah = $('#modal-nilai-salah').val();
            $.ajax({
                url: '/level-soal/' + id,
                method: 'POST',
                dataType: "JSON",
                data: {
                    level: level,
                    nilai_benar: nilai_benar,
                    nilai_salah: nilai_salah,
                    '_method': 'PUT',
                    "_token": "{{ csrf_token() }}",
                },
                success: function(result) {
                    // console.log('BERHASIL!');
                    // console.log(result);
                    $('#modal-edit').modal('toggle');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Berhasil merubah data',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function() {
                        // console.log(result.id.id);
                        $('#lid' + result.id.id + ' #row-level').text(result.id.level);
                        $('#lid' + result.id.id + ' #row-nilai-benar').text(result.id.nilai_benar);
                        $('#lid' + result.id.id + ' #row-nilai-salah').text(result.id.nilai_salah);
                        // window.location = "/level-soal";
                    });
                },
                error: function(request, error) {
                    // console.log(error);
                    // console.log(arguments);
                    let err_nilai_benar = arguments[0].responseJSON.error.nilai_benar ? arguments[0]
                        .responseJSON.error.nilai_benar[0] : '';
                    let err_nilai_salah = arguments[0].responseJSON.error.nilai_salah ? arguments[0]
                        .responseJSON.error.nilai_salah[0] : '';
                    let err_level = arguments[0].responseJSON.error.level ? arguments[0].responseJSON
                        .error.level[0] : '';

                    if (err_level) {
                        $('#level-error').removeClass('d-none');
                        $('#level-error').text(err_level);
                        $('#level-error').addClass('invalid-feedback');
                        $('#modal-level').addClass('is-invalid');
                    }

                    if (err_nilai_benar) {
                        $('#nilai_benar-error').removeClass('d-none');
                        $('#nilai_benar-error').text(err_nilai_benar);
                        $('#nilai_benar-error').addClass('invalid-feedback');
                        $('#modal-nilai-benar').addClass('is-invalid');
                    }

                    if (err_nilai_salah) {
                        $('#nilai_salah-error').removeClass('d-none');
                        $('#nilai_salah-error').text(err_nilai_benar);
                        $('#nilai_salah-error').addClass('invalid-feedback');
                        $('#modal-nilai-benar').addClass('is-invalid');
                    }
                }
            })
        });
    </script>
@endsection
