@extends('layouts.main')

@section('content')
    <div class="container py-4">
        @if($sekolahs->count() == 0)
            <button class="btn btn-warning" id="buttonImportSekolah">Import Sekolah</button>
        @endif
        @if($users->count() == 1)
            <button class="btn btn-primary" id="buttonImport">Import Users</button>
        @else
        <form action="/reset-table-users" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger mb-2 mt-1" id="buttonReset">Reset Users & Sekolah</button>
        </form>
        @endif
    </div>

    <script>
        $('#buttonImport').click(function() {
            $.ajax({
                headers: {
                    "Authorization": "Bearer 272|BkAyGwPDSxkx0ChHpAPZvGTYC1mFQFGVtlam8AqA"
                },
                url: 'https://api-ncc.chemweekits.com/api/userall',
                type: 'GET',
                success: function(result) {
                    console.log(result);
                    $.ajax({
                        url: '/import-tables',
                        type: 'POST',
                        data: {
                            '_token': "{{ csrf_token() }}",
                            users: result.users,
                        },
                        success: function(res) {
                            console.log(res);
                        }
                    });
                    Swal.fire({
                        icon : 'success',
                        text : 'Berhasil import data users',
                        timer : 3000
                    }).then(() => {
                        location.reload();
                    })
                }
            })
        });

        $('#buttonImportSekolah').click(function() {
            $.ajax({
                headers: {
                    "Authorization": "Bearer 272|BkAyGwPDSxkx0ChHpAPZvGTYC1mFQFGVtlam8AqA"
                },
                url: 'https://api-ncc.chemweekits.com/api/userall',
                type: 'GET',
                success: function(result) {
                    console.log(result);
                    $.ajax({
                        url: '/import-sekolah',
                        type: 'JSON',
                        method: 'POST',
                        data: {
                            '_token': "{{ csrf_token() }}",
                            sekolahs : result.sekolahs
                        },
                        success: function(res) {
                            console.log(res);
                        }
                    });
                    Swal.fire({
                        icon : 'success',
                        text : 'Berhasil import data users',
                        timer : 3000
                    }).then(() => {
                        location.reload();
                    })
                }
            })
        });
    </script>
@endsection
