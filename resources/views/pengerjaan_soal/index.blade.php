@extends('layouts.main')

@section('timer')
<div class="bg-warning shadow-sm mb-5 sticky-top">
    <div class="d-flex align-items-center">
        <div class="col-2 text-center border-end border-2 border-dark rounded-pill me-auto">
            <h5>{{ $title }}</h5>
        </div>
        <div class="col-2 text-center border-start border-2 border-dark rounded-pill ms-auto">
            <h5 id="sisaWaktu"></h5>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="container py-4">
        <div class="row justify-content-center">
            <div class="col col-md-12 text-center">
                <button class="btn rounded-pill text-bg-danger mb-4" type="button" id="buttonSelesai">Selesai</button>
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h3>Pilihan Ganda</h3>
                        @foreach ($soal_pilihan_gandas as $soal_pilihan_ganda)
                            <button type="button" id="nomor-soal"
                                class="btn btn-sm btn-primary shadow-sm mt-2 button-soal {{ $jawaban_pg->where(['soal_pilihan_ganda_id' => $soal_pilihan_ganda->id, 'user_id' => auth()->user()->id])->first() ? ($jawaban_pg->where(['soal_pilihan_ganda_id' => $soal_pilihan_ganda->id, 'user_id' => auth()->user()->id])->first()->jawaban ? 'btn-success' : '') : '' }}"
                                data-idsoal="{{ $soal_pilihan_ganda->id }}"
                                data-tahapsoal="{{ $soal_pilihan_ganda->tahap_soal_id }}"
                                data-typesoal="pg">{{ $soal_pilihan_ganda->nomor_soal >= 10 ? $soal_pilihan_ganda->nomor_soal : '0' . $soal_pilihan_ganda->nomor_soal }}</button>
                            @if ($soal_pilihan_ganda->nomor_soal == round($total_soal_pg / 2))
                                <br>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <div class="card-body">
                        <h3>Essay</h3>
                        @foreach ($soal_essays as $soal_essay)
                            <button type="button" id="nomor-soal"
                                class="btn btn-sm btn-primary shadow-sm mt-2 button-soal {{ $jawaban_essay->where(['soal_essay_id' => $soal_essay->id, 'user_id' => auth()->user()->id])->first() ? ($jawaban_essay->where(['soal_essay_id' => $soal_essay->id, 'user_id' => auth()->user()->id])->first()->jawaban ? 'btn-success' : '') : '' }}"
                                data-idsoal="{{ $soal_essay->id }}" data-tahapsoal="{{ $soal_essay->tahap_soal_id }}"
                                data-typesoal="essay">{{ $soal_essay->nomor_soal >= 10 ? $soal_essay->nomor_soal : '0' . $soal_essay->nomor_soal }}</button>
                            @if ($soal_essay->nomor_soal == round($total_soal_essay / 2))
                                <br>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 mt-2 mt-md-0 col-md-8">
                <div class="card shadow-sm d-none papansoal">
                    <div class="card-body" style="background: #F8F8F8">
                        <h3 class="nomorsoal"></h3>
                        <div class="card">
                            <div class="card-body" id="card-soal"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-2 mt-md-0 col-md-4">
                <div class="card shadow-sm d-none papansoal">
                    <div id="card-jawaban"></div>
                    <div class="card-body btn-group-vertical" role="group" aria-label="Basic outlined example" id="card-radio">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal lihat gambar start --}}
    <div class="modal fade" id="modal-lihat-gambar" tabindex="-1" aria-labelledby="modal-lihat-gambar" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Gambar Jawaban</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="" alt="gambar-jawaban" style="width: 100%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal lihat gambar end --}}

    {{-- modal lihat gambar soal-pg start --}}
    <div class="modal fade" id="imageOpsiPg" tabindex="-1" aria-labelledby="imageOpsiPg" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Gambar Jawaban</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="modal-images">
                        <img width="100%" src="" alt="opsiImage" class="images-modal" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal lihat gambar soal-pg end --}}

    <script>
        function getSecondPart(str) {
            return str.split('.')[1];
        }

        $('body').delegate('.opsiImage', 'click', function(){
            console.log($(this).attr('src'));
            $('.images-modal').attr('src', $(this).attr('src'));
        });

        function toast(title) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: title
            })
        }

        $('#buttonSelesai').click(function() {
            Swal.fire({
                title: 'Apa kamu yakin sudah selesai?',
                text: "Silahkan ketik 'SELESAI' untuk konfirmasi",
                input: 'text',
                icon: 'warning',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Selesai',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    console.log(text);
                    if (text != 'SELESAI') {
                        Swal.fire({
                            title: 'Gagal!',
                            text: 'Input tidak sesuai!',
                            icon: 'warning'
                        })
                    } else {
                        let timerInterval
                        Swal.fire({
                            title: 'Memeriksa Jawaban',
                            html: 'Sedang Menyimpan data.',
                            timerProgressBar: true,
                            timer: 5000,
                            didOpen: () => {
                                Swal.showLoading()
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            if (result.dismiss === Swal.DismissReason.timer) {
                                Swal.fire({
                                    title: 'Berhasil!',
                                    text: 'Data berhasil tersimpan',
                                    icon: 'success',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then(function() {
                                    $.ajax({
                                        url: '/selesai',
                                        type: 'POST',
                                        data: {
                                            tahap_soal_id: {{ $tahap->id }},
                                            '_token': "{{ csrf_token() }}"
                                        },
                                        success: function(result) {
                                            window.location = "/";
                                        },
                                        error: function(request, error) {
                                            Swal.fire({
                                                title: 'Gagal!',
                                                text: 'Silahkan dicoba lagi',
                                                icon: 'warning',
                                                timer: 5000,
                                                showConfirmButton: false
                                            })
                                        }
                                    });
                                });
                                console.log('finished')
                            }
                        })
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()
            })
        });

        $('body').delegate('#card-radio button', 'click', function() {
            let huruf = $(this).data('huruf');
            let isian = $(this).data('isian');
            let idsoal = $(this).data('idsoal');
            $.ajax({
                url: '/pengerjaan',
                type: 'POST',
                typeData: 'JSON',
                data: {
                    huruf: huruf,
                    isian: isian,
                    idsoal: idsoal,
                    '_token': "{{ csrf_token() }}"
                },
                success: function(result) {
                    // console.log(result);
                    toast('Data berhasil tersimpan');
                    $(`.button-soal[data-idsoal="${idsoal}"][data-typesoal="pg"]`).removeClass(
                        'btn-warning');
                    $(`.button-soal[data-idsoal="${idsoal}"][data-typesoal="pg"]`).addClass(
                        'btn-success');
                },
                error: function(request, error) {
                    if (arguments[0].status == 403) {
                        Swal.fire({
                            title: 'Gagal!',
                            text: 'Sesi ujian telah selesai.',
                            icon: 'warning',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                            }
                        }).then(function() {
                            window.location = '/jadwal';
                        })
                    }
                }
            })
        });

        $('.button-soal').click(function() {
            let nomor = $(this).text();
            let idSoal = $(this).data('idsoal');
            let tahapSoal = $(this).data('tahapsoal');
            let typeSoal = $(this).data('typesoal');
            if (!$(this).hasClass('btn-success')) {
                $(this).addClass('btn-warning');
            }
            $('.button-soal').removeClass('border border-5 border-danger');
            $(this).addClass('border border-5 border-danger');
            if (typeSoal == 'pg') {
                $.ajax({
                    url: '/pengerjaan',
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        nomor: nomor,
                        idSoal: idSoal,
                        tahapSoal: tahapSoal
                    },
                    success: function(result) {
                        console.log(result);
                        $('.info-level').empty();
                        $('.papansoal').removeClass('d-none');
                        $('#card-soal').parent().before(`
                            <div class="info-level">
                                <small>Level soal : <span class="fw-bold">${result.soal.level_soal.level}</span></small> |
                                <small>Nilai benar : <span class="fw-bold text-success">${result.soal.level_soal.nilai_benar}</span></small> |
                                <small>Nilai salah : <span class="fw-bold text-danger">${result.soal.level_soal.nilai_salah}</span></small>
                            </div>
                        `);
                        $('#card-soal').html(result.soal.soal);
                        $('#card-jawaban').empty();
                        $('#card-radio').empty();
                        $('#card-jawaban').html(
                            `<h3 class="fw-bold text-center mb-3" id="text-jawaban"><ins>Jawaban</ins></h3>`
                        );
                        $.each(result.opsi, function(index, value) {
                            let checked = (result.jawaban ? (result.jawaban.jawaban == value.huruf ? 'btn-outline-primary bg-info' : 'btn-outline-primary') : 'btn-outline-primary');
                            let img = `<a href="javascript:;"><img data-bs-toggle="modal" data-bs-target="#imageOpsiPg" width="100%" class="mb-5 text-center opsiImage" src="/images/soal-pg/${value.image}"></a>`;
                            $('#card-radio').append(`
                        <button type="button" class="btn ${checked} text-start" data-huruf="${value.huruf}" data-isian="${value.isi_pilihan_jawaban}" data-idsoal="${value.soal_pilihan_ganda_id}">${value.huruf}. ${value.isi_pilihan_jawaban ?? ''}</button> ${value.image ? img : ''}
                    `)
                        })
                    }
                })
            } else if (typeSoal == 'essay') {
                $.ajax({
                    url: '/pengerjaan',
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        nomor: nomor,
                        idSoal: idSoal,
                        tahapSoal: tahapSoal,
                        typeSoal: typeSoal
                    },
                    success: function(result) {
                        // console.log(getSecondPart(result.jawaban.jawaban_file_upload));
                        console.log(result);
                        $('.info-level').empty();
                        $('.papansoal').removeClass('d-none');
                        $('#card-soal').html(result.soal.soal)
                        $('#card-jawaban').empty();
                        $('#card-radio').empty();
                        $('#card-jawaban').html(
                            `<h3 class="fw-bold text-center mb-3" id="text-jawaban" data-idsoal="${result.soal.id}"><ins>Jawaban</ins></h3>`
                        );
                        $('#card-jawaban').append(`
                            <form method="POST" enctype="multipart/form-data" id="form-submit-essay">
                                <textarea class="form-control ckeditor" placeholder="Isi jawaban disini.." id="jawaban-essay" name="jawaban">${(result.jawaban) ? result.jawaban.jawaban : ''}</textarea><br>
                                <label for="jawaban-file-upload">Select file</label>
                                <input class="form-control" type="file" id="jawaban-file-upload" name="jawaban_file_upload" >
                                ${(result.jawaban) ? (result.jawaban.jawaban_file_upload ? `<button class="border-0 mt-3">${getSecondPart(result.jawaban.jawaban_file_upload) == 'pdf' ? `<a target="_blank" href="/images/jawaban-essay/${(result.jawaban) ? result.jawaban.jawaban_file_upload : 'no pict.png'}" data-idjawaban="${result.jawaban.id}" data-gambar="${(result.jawaban) ? result.jawaban.jawaban_file_upload : 'no pict.png'}" id="lihat-gambar">open PDF</a>` : `<img height="100px" width="100px" src="/images/jawaban-essay/${(result.jawaban) ? result.jawaban.jawaban_file_upload : 'no pict.png'}" data-idjawaban="${result.jawaban.id}" data-gambar="${(result.jawaban) ? result.jawaban.jawaban_file_upload : 'no pict.png'}" id="lihat-gambar" data-bs-toggle="modal" data-bs-target="#modal-lihat-gambar">`}</button> <br> <button class="badge bg-danger border-0 type="button id="hapusGambar">delete</button>` : '') : ''}
                                <button type="submit" class="badge bg-${result.warna_tombol} border-0 float-end mt-2" id="button-submit-essay">${result.tombol_essay}</button>
                            </form>
                    `).ready(function() {
                            $('#lihat-gambar').click(function(e) {
                                if(getSecondPart(result.jawaban.jawaban_file_upload) != 'pdf'){
                                    e.preventDefault();
                                }
                                let gambar = $(this).attr('data-gambar');
                                console.log(gambar);
                                $('.modal-body img').attr('src', '/images/jawaban-essay/' + gambar);
                            })

                            $('#hapusGambar').click(function(e) {
                                e.preventDefault();
                                let gambar = $(this).parent().find('#lihat-gambar').data('gambar');
                                let id = $(this).parent().find('#lihat-gambar').data('idjawaban');
                                const url = '/jawaban-soal-essay/' + id;
                                Swal.fire({
                                    title: 'Anda yakin akan dihapus?',
                                    text: "Gambar akan dihapus",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yakin!',
                                    cancelButtonText: 'Nanti Dulu!',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        $.ajax({
                                            url: url,
                                            method: 'DELETE',
                                            data: {
                                                '_token': "{{ csrf_token() }}"
                                            },
                                            success: function(result) {
                                                console.log(result);
                                            }
                                        }).then(function() {
                                            console.log('ok');
                                            $('#lihat-gambar').remove();
                                            $('#hapusGambar').remove();
                                            toast('gambar berhasil dihapus');
                                        })
                                    }
                                })
                            })

                            $('#form-submit-essay').submit(function(e) {
                                e.preventDefault();
                                var formData = new FormData(this);
                                var idSoal = $('#text-jawaban').data('idsoal');
                                console.log(idSoal);

                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': "{{ csrf_token() }}",
                                    },
                                    url: '/jawaban-essay/' + idSoal,
                                    type: 'POST',
                                    data: formData,
                                    cache: false,
                                    processData: false,
                                    contentType: false,
                                    success: function(result) {
                                        console.log(result);
                                        toast('Data berhasil tersimpan');
                                        $('#lihat-gambar').attr('src',
                                            '/images/jawaban-essay/' + result
                                            .jawaban_file_upload);
                                        $('#lihat-gambar').attr(
                                            'data-gambar', result
                                            .jawaban_file_upload);
                                        $('#jawaban-essay').text(result
                                            .jawaban);
                                        $('#validationErrMessage').empty();
                                        $(`.button-soal[data-idsoal="${idSoal}"][data-typesoal="essay"]`)
                                            .removeClass(
                                                'btn-warning');
                                        $(`.button-soal[data-idsoal="${idSoal}"][data-typesoal="essay"]`)
                                            .addClass(
                                                'btn-success');
                                        // $('#button-submit-essay').before(`
                                    //     <p class="rounded-pill float-end animate__ animate__fadeIn">✓</p>
                                    // `);
                                        $('#card-jawaban').addClass('p-2');
                                    },
                                    error: function(request, error) {
                                        if (arguments[0].status == 400) {
                                            let messageErr = arguments[0]
                                                .responseJSON.message
                                                .jawaban_file_upload[0];
                                            console.log(messageErr);
                                            $('#jawaban-file-upload').after(`
                            <small class="text-danger fst-italic" id="validationErrMessage">${messageErr}</small>
                        `)
                                        }
                                    }
                                });
                            })
                        })
                        $('#card-jawaban').addClass('p-2');
                    }
                })
            }
        });
    </script>

    <script>
        // Update the count down every 1 second
        var tahap_id = {{ auth()->user()->tahap_soal->id }};
        var onGoing = setInterval(function() {
            var countDownDate = new Date("{{ $tahap->tanggal_ujian }} {{ $tahap->jam_mulai }}").getTime();
            var akhirUjian = new Date(countDownDate + {{ $tahap->durasi_ujian }} * 60000).getTime();
            var now = new Date().getTime();
            var durasi = akhirUjian - now;

            var days = Math.floor(durasi / (1000 * 60 * 60 * 24));
            var hours = Math.floor((durasi % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((durasi % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((durasi % (1000 * 60)) / 1000);

            $('#sisaWaktu').html(hours + "h " + minutes + "m " + seconds + "s ");
            $('#sisaWaktu').addClass('text-danger fw-bold fst-italic');

            // If the count down is finished, write some text
            if (durasi < 0) {
                $("#sisaWaktu").html(`
                    <p class="text-danger fw-bold fst-italic">Selesai</p>
                `);
                Swal.fire({
                    title: 'Selesai!',
                    text: 'Sesi ujian telah selesai.',
                    icon: 'warning',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location = "/jadwal";
                });
                $('#sisaWaktuLi').addClass('d-none');
                $.ajax({
                    url: '/jadwal',
                    method: 'POST',
                    data: {
                        id: tahap_id,
                        status: 'selesai',
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#status-ujian').text(result.status_soal);
                        $('#status-ujian').addClass('text-danger');
                    },
                    error: function(request, error) {
                        console.log(arguments[0].responseText);
                    }
                })
                setTimeout(clearInterval(onGoing), 2000);
            }
        }, 1000);
    </script>
@endsection
