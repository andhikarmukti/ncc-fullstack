@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col col-md-6 border rounded-5 p-5 shadow-sm">
                <form action="/peserta-register" method="POST">
                    @csrf
                    <div class="mb-3">
                      <label for="name" class="form-label">Name</label>
                      <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                      <label for="no_hp" class="form-label">No HP</label>
                      <input type="number" class="form-control" id="no_hp" name="no_hp">
                    </div>
                    <div class="mb-3">
                      <label for="email" class="form-label">Email</label>
                      <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="mb-3">
                      <label for="password" class="form-label">Password</label>
                      <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="col-8 mx-auto mt-5 mb-5">
            <h3 class="text-center fst-italic fw-bold">Daftar Uji Coba</h3>
            <table class="table table-hover" id="table">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Score</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($users as $user)
                    <tr>
                      <th scope="row">{{ $i++ }}</th>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->rankings->where('tahap_soal_id', 3)->first() ? $user->rankings->where('tahap_soal_id', 3)->first()->total_score : 0 }}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#table').DataTable()
        })
    </script>
@endsection