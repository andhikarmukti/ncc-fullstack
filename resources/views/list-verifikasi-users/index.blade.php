@extends('layouts.main')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <h3 class="text-center">Daftar User Terverifikasi</h3>
            <table class="table table-hover table-striped" id="tableVerifUser">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Asal Sekolah</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->name }}</th>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->sekolah->asal_sekolah ?? 'belum isi data sekolah' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="/vendor/dataTables/datatables.js"></script>
<script>
    $('#tableVerifUser').DataTable({
         responsive: true
            , buttons: ['print', 'excel', 'pdf', 'colvis']
            , dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-5'i><'col-md-7'p>>"
            , lengthMenu: [
                [5, 10, 15, 25, 50, -1]
                , [5, 10, 15, 25, 50, "All"]
            ]

    })
</script>
@endsection
