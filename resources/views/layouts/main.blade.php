<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NCC - {{ $title }}</title>
    {{-- Bootstrap 5 --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    {{-- My CSS --}}
    <link rel="stylesheet" href="/css/style.css">
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- CSS DataTables --}}
    <link rel="stylesheet" href="/vendor/dataTables/datatables.css">
    {{-- Animate CSS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    {{-- Dashboard CSS --}}
    @if ($title == 'Home')
        <link rel="stylesheet" href="/css/dfstyle.css">
    @endif
    {{-- Font Awsome --}}
    <script src="https://kit.fontawesome.com/61147ab122.js" crossorigin="anonymous"></script>
</head>

<body>
    @include('sweetalert::alert')
    @auth
    <div class="sticky-top">
        <nav class="navbar navbar-dark navbar-expand-lg bg-dark sticky-top" style="z-index:1092;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">NCC</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('/') ? 'active soft-black rounded text-light' : '' }}"
                                aria-current="page" href="/">Dashboard</a>
                        </li>
                        @can('admin')
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle {{ request()->is('*soal*') ? 'active soft-black rounded text-light' : '' }}"
                                    href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    Kelola Soal
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item {{ request()->is('*tahap-soal*') ? 'active text-light' : '' }}"
                                            aria-current="page" href="/tahap-soal">Tahap Soal</a></li>
                                    <li><a class="dropdown-item {{ request()->is('*level-soal*') ? 'active text-light' : '' }}"
                                            aria-current="page" href="/level-soal">Level Soal</a></li>
                                    <hr>
                                    <li><a class="dropdown-item {{ request()->is('*soal-pilihan-ganda*') ? 'active text-light' : '' }}"
                                            href="/soal-pilihan-ganda">Soal Pilihan Ganda</a></li>
                                    <li><a class="dropdown-item {{ request()->is('soal-essay') ? 'active text-light' : '' }}"
                                            href="/soal-essay">Soal Essay</a></li>
                                    <hr>
                                    <li><a class="dropdown-item {{ request()->is('*jawaban-soal-essay*') ? 'active text-light' : '' }}"
                                            href="/jawaban-soal-essay">Periksa Soal Essay</a></li>
                                    <li><a class="dropdown-item {{ request()->is('*jawaban-soal-pg*') ? 'active text-light' : '' }}"
                                            href="/jawaban-soal-pg">Jawaban Soal Pilihan Ganda</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('*ranking*') ? 'active soft-black rounded text-light' : '' }}"
                                    href="/ranking">Ranking</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*jadwal*') ? 'active soft-black rounded text-light' : '' }}"
                                href="/jadwal">Jadwal</a>
                        </li>
                    </ul>
                    <div class="dropdown ms-auto">
                        <button class="btn text-light dropdown-toggle" type="button" id="profile"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa-solid fa-user"></i>&nbsp;&nbsp; {{ auth()->user()->name }}
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="profile">
                            <li><div class="dropdown-item" href="#">
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                        <button type="submit" class="dropdown-item border-0 text-dark"
                                            href="#">Logout</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        @yield('timer')
    </div>
    @endauth
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @yield('content')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>
    {{-- Datatables --}}
    <script src="/vendor/dataTables/datatables.js"></script>
    {{-- CKEditor --}}
    <script src="//cdn.ckeditor.com/4.19.0/full/ckeditor.js"></script>
</body>

</html>
