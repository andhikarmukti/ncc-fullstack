@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <h1 class="text-center my-4">Jadwal Perlombaan</h1>
            <div class="col col-lg-6">
                <div class="card shadow-sm rounded">
                    <div class="card-header text-center bg-info fw-bold">
                        Tahap lomba
                    </div>
                    <div class="row align-items-center">
                        <div class="col-6">
                            <h1 class="text-center" id="tryouts">{{ auth()->user()->tahap_soal->tahap_soal }}</h1>
                        </div>
                        <div class="col-6">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item text-center">
                                    <span
                                        class="fst-italic">{{ date_format(date_create(auth()->user()->tahap_soal->tanggal_ujian), 'l, d/m/Y') }}</span>
                                </li>
                                <li class="list-group-item">Jam : <span
                                        class="fw-bold">{{ auth()->user()->tahap_soal->jam_mulai }}</span>
                                </li>
                                <li class="list-group-item">Durasi :
                                    <span class="fw-bold">{{ auth()->user()->tahap_soal->durasi_ujian }} menit</span>
                                </li>
                                <li class="list-group-item">Status Ujian :
                                    <span id="status-ujian"
                                        class="fw-bold fst-italic text-primary">{{ auth()->user()->tahap_soal->status_soal }}</span>
                                </li>
                                <li class="list-group-item d-none" id="sisaWaktuLi">Sisa Waktu :
                                    <p class="fw-bold d-inline fs-5" id="sisaWaktu"></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-6 mt-2 text-center">
                <h3 id="timer" class="border-1 shadow-sm rounded p-3"></h3>
            </div>
        </div>
    </div>

    <script>
        // Update the count down every 1 second
        var tahap_id = {{ auth()->user()->tahap_soal->id }};
        var soon = setInterval(function() {
            var countDownDate = new Date("{{ $tahap->tanggal_ujian }} {{ $tahap->jam_mulai }}").getTime();
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var akhirUjian = new Date(countDownDate + {{ $tahap->durasi_ujian }} * 60000).getTime();

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            $("#timer").html(days + " hari " + hours + " jam " + minutes + " menit " + seconds + " detik ");

            // If the count down is finished, write some text
            if (distance < 0) {
                $("#timer").addClass('d-none');
                $("#timer").before(`
                    <a id="mulai" class="border-1 shadow-sm rounded btn btn-primary mt-2" href="/pengerjaan">Mulai</a><br>
                `);
                $('#sisaWaktuLi').removeClass('d-none');
                $.ajax({
                    url: '/jadwal',
                    method: 'POST',
                    data: {
                        id: tahap_id,
                        status: 'mulai',
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#status-ujian').text(result.status_soal);
                        $('#status-ujian').addClass('text-success');
                    },
                    error: function(request, error) {
                        console.log(arguments[0].responseText);
                    }
                })
                setTimeout(clearInterval(soon), 2000);
            }
        }, 1000);

        // Update the count down every 1 second
        var onGoing = setInterval(function() {
            var countDownDate = new Date("{{ $tahap->tanggal_ujian }} {{ $tahap->jam_mulai }}").getTime();
            var akhirUjian = new Date(countDownDate + {{ $tahap->durasi_ujian }} * 60000).getTime();
            var now = new Date().getTime();
            var durasi = akhirUjian - now;

            var days = Math.floor(durasi / (1000 * 60 * 60 * 24));
            var hours = Math.floor((durasi % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((durasi % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((durasi % (1000 * 60)) / 1000);

            $('#sisaWaktu').html(hours + "h " + minutes + "m " + seconds + "s ");
            $('#sisaWaktu').addClass('text-danger fw-bold fst-italic');

            // If the count down is finished, write some text
            if (durasi < 0) {
                $("#timer").addClass('d-none');
                $("#mulai").addClass('d-none');
                $("#timer").before(`
                    <h1 class="mt-2 text-danger fw-bold fst-italic">{{ $tahap->tahap_soal }} Selesai</h1><br>
                `);
                $('#sisaWaktuLi').addClass('d-none');
                $.ajax({
                    url: '/jadwal',
                    method: 'POST',
                    data: {
                        id: tahap_id,
                        status: 'selesai',
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#status-ujian').text(result.status_soal);
                        $('#status-ujian').addClass('text-danger');
                    },
                    error: function(request, error) {
                        console.log(arguments[0].responseText);
                    }
                })
                setTimeout(clearInterval(onGoing), 2000);
            }
        }, 1000);
    </script>
@endsection
