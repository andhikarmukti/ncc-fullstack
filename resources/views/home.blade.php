@extends('layouts.main')

@section('content')
    @can('admin')
        <div class="p-4 crystal text-white">
            <div class="w-44 mx-auto ">
                <img src="/images/monitoring.png" alt="" class="w-44">
            </div>

            <div class="rounded text-center py-4 my-4 border ">
                <h2 class="fw-bold">Jumlah Peserta</h2>
                <h3>{{ $user->count() }}</h3>
            </div>

            <div class="text-center my-4 ">
                <h2 class="fw-bold">Peserta Yang Lulus :</h2>
            </div>

            <div class="row mb-5">
                <div class="col-12 col-md-4 mb-4 mb-md-0 text-center">
                    <div class="rounded bg-success py-2 shadow text-white">
                        <h3 class="fw-bold my-2">Tahap tryout</h3>
                        <h3>{{ $user->where('tahap_soal_id', 1)->count() }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 mb-md-0 text-center">
                    <div class="rounded bg-primary py-2 shadow text-white">
                        <h3 class="fw-bold my-2">Tahap penyisihan 1</h3>
                        <h3>{{ $user->where('tahap_soal_id', 2)->count() }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 mb-md-0 text-center">
                    <div class="rounded bg-danger py-2 shadow text-white">
                        <h3 class="fw-bold my-2">Tahap penyisihan 2</h3>
                        <h3>{{ $user->where('tahap_soal_id', 3)->count() }}</h3>
                    </div>
                </div>
            </div>

            <div class="rounded text-center py-4 my-4 border">
                <h2 class="fw-bold">Peserta Yang tidak lulus</h2>
                <h3>{{ $user->where('tidak_lulus', 3)->count() }}</h3>
            </div>
        </div>
    @endcan

    @can('user')
        <div>
            <div class=" h-44 bg-purle">
                <div class="w-44 mx-auto ">
                    <img src="/images/ncc.png" alt="" class="w-44 pt-20 text-center">
                </div>
                <div class="w-5 mt-5 fixed right-2 top-10">
                    <img src="/images/scroll.png" alt="" class="w-5">
                </div>
            </div>

            {{-- <div class="mt-5 mb-10">
                <h1 id="countdown" class="text-center gradient-text"></h1>
            </div> --}}
            <div class="w-screen bg-griy p-5">
                <h1 class="text-center fst-italic fw-bold">RUMUS PENTING!</h1>
                <div class="container">
                    <div class="d-flex flex-column flex-md-row justify-content-between">
                        <a href="/images/rumus1.jpg" target="_blank" class="shadow mb-1"><img width="300" src="/images/rumus1.jpg" alt="rumus1"></a>
                        <a href="/images/rumus2.jpg" target="_blank" class="shadow mb-1"><img width="300" src="/images/rumus2.jpg" alt="rumus2"></a>
                        <a href="/images/rumus3.jpg" target="_blank" class="shadow mb-1"><img width="300" src="/images/rumus3.jpg" alt="rumus3"></a>
                    </div>
                </div>
            </div>
            <div class="w-screen bg-griy p-5">
                <div class="w-40 d-flex ms-0 ms-sm-5 mb-3">
                    <img src="/images/infopengerjaanpg.png" alt="" class="w-40">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <img src="/images/infopg.png" alt="" class="w-44">
                        </div>

                        <div class="col-sm  my-3">
                            <h1></h1>
                            <div class="d-flex">
                                <div class="rounded w-5 h-5 bg-primary"></div>
                                <p class="py-2 px-2">Belum pernah dilihat dan belum diisi</p>
                            </div>
                            <div class="d-flex">
                                <div class="rounded w-5 h-5 border border-3 border-danger"></div>
                                <p class="py-2 px-2">Sedang berada di soal saat ini</p>
                            </div>
                            <div class="d-flex">
                                <div class="rounded w-5 h-5 bg-success"></div>
                                <p class="py-2 px-2">Sudah pernah dilihat dan sudah terisi</p>
                            </div>
                            <div class="d-flex">
                                <div class="rounded w-5 h-5 bg-warning"></div>
                                <p class="py-2 px-2">Sudah pernah dilihat tapi belum terisi</p>
                            </div>
                            <div class="">
                                <p class="fw-bold">Note :</p>
                                <p class="">Ketika kalian mengeklik jawaban, jawaban akan otomatis terkirim. ditandai
                                    dengan nomor soal berubah menjadi hijau</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-screen bg-griy p-5">
                <div class="w-40 d-flex ms-0 ms-sm-5 mb-3">
                    <img src="/images/infopengerjaanesai.png" alt="" class="w-40">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <img src="/images/infoesai.png" alt="" class="w-44">
                        </div>

                        <div class="col-sm">
                            <h3 class="text-center my-4 border border-0 border-bottom-2 text-purple"> Yang harus diperhatikan
                                ketika pengerjaan essai</h3>
                            <div class="d-flex">
                                <p class="py-2 px-2 fw-bold">ketika mengupload gambar maksimal 50 mb </p>
                            </div>
                            <div class="d-flex">
                                <p class="py-2 px-2 fw-bold">Gambar yang di upload harus jelas</p>
                            </div>
                            <div class="d-flex">
                                <p class="py-2 px-2 fw-bold">Siapkan USB kabel untuk mengirim gambar dari hp ke pc </p>
                            </div>
                            <div class="d-flex">
                                <p class="py-2 px-2 fw-bold">Bisa Juga lewat via google drive diupload terlebih dahulu di
                                    google drive kemudian di download kembali di pc</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">

                <div>
                    <h1 class="text-center gradient-text fw-bold my-5">PERHATIAN</h1>
                </div>
                <div class="row mb-5">
                    <div class="col-12 col-md-4 mb-4 mb-md-0">
                        <div class="w-20 mx-auto">
                            <img src="/icons/wifi.png" alt="" class="w-20 mx-auto">
                        </div>
                        <h5 class="text-center my-3">Pehatikan Jaringan Sebelum Mengerjakan</h5>
                    </div>
                    <div class="col-12 col-md-4 mb-4 mb-md-0">
                        <div class="w-20 mx-auto">
                            <img src="/icons/essay.png" alt="" class="w-20 mx-auto">
                        </div>
                        <h5 class="text-center my-3">Siapkan Selembar kertas untuk mengerjakan soal</h5>
                    </div>
                    <div class="col-12 col-md-4 mb-4 mb-md-0">
                        <div class="w-20 mx-auto">
                            <img src="/icons/checklist.png" alt="" class="w-20 mx-auto">
                        </div>
                        <h5 class="text-center my-3">Pastikan Jawaban yang dipilih sudah benar</h5>
                    </div>

                </div>
            </div>
        </div>
        {{-- <script>
            var countDownDate = new Date("{{ auth()->user()->tahap_soal->tanggal_ujian }} {{ auth()->user()->tahap_soal->jam_mulai }}").getTime();

            var x = setInterval(function() {

                var now = new Date().getTime();

                var distance = countDownDate - now;

                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                document.getElementById("countdown").innerHTML = days + ": " + hours + ": " +
                    minutes + ": " + seconds + " S ";

                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("countdown").innerHTML = "EXPIRED";
                }
            }, 1000);
        </script> --}}
    @endcan
@endsection
