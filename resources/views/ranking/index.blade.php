@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <h1 class="text-center mb-5">Ranking</h1>
        {{-- Pilihan Tahap Soal Start --}}
        <form action="/ranking" method="GET">
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <div class="card p-2 shadow-sm mb-3">
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-6 col-md-6">
                                    <select class="form-select" aria-label="Default select example" name="tsi">
                                        <option selected disabled>Pilih Tahap Soal</option>
                                        @foreach ($tahap_soals as $tahap_soal)
                                            <option value="{{ $tahap_soal->id }}"
                                                {{ request('tsi') == $tahap_soal->id ? 'selected' : '' }}>
                                                {{ $tahap_soal->tahap_soal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="row">
                                        <button class="btn btn-primary" type="submit">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        {{-- Pilihan Tahap Soal End --}}

        <div class="row justify-content-center mt-2">
            <div class="col-9">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover border border-1" id="tableRanking">
                                <thead class="text-bg-dark">
                                    <tr>
                                        <th scope="col">Rank</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Tahap Ujian</th>
                                        <th scope="col">Total Score</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($rankings as $ranking)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $ranking->user->name ?? 'anonymous' }}</td>
                                            <td>{{ $ranking->tahap_soal->tahap_soal }}</td>
                                            <td class="fw-bold">{{ $ranking->total_score }}</td>
                                            <td id="status-{{ $ranking->id }}"
                                                class="fw-bold fst-italic {{ $ranking->status == 1 ? 'text-success' : 'text-danger' }}">
                                                {{ $ranking->status == '' ? '?' : ($ranking->status == 1 ? 'lulus' : 'tidak lulus') }}
                                            </td>
                                            <td>
                                                <a href="#"><span class="badge bg-success lulus"
                                                        data-idranking="{{ $ranking->id }}">lulus</span></a>
                                                <a href="#"><span class="badge bg-danger tidaklulus"
                                                        data-idranking="{{ $ranking->id }}">tidak
                                                        lulus</span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tableRanking').DataTable({
                responsive: true,
                buttons: ['print', 'excel', 'pdf'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                lengthMenu: [
                    [10, 15, 25, 50, -1],
                    [10, 15, 25, 50, "All"]
                ],
                columnDefs: [{
                        className: "dt-head-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    },
                    {
                        className: "dt-body-center",
                        targets: [0, 1, 2, 3, 4, 5]
                    }
                ]
            });
        });

        $('.lulus').click(function() {
            let idRanking = $(this).data('idranking');
            console.log(idRanking);
            $.ajax({
                url: '/ranking/' + idRanking,
                type: 'POST',
                data: {
                    status: 1,
                    '_method': 'PUT',
                    '_token': "{{ csrf_token() }}"
                },
                success: function(result) {
                    $('#status-' + idRanking).animate({
                        'opacity': 0
                    }, 1500, function() {
                        $(this).html('lulus').animate({
                            'opacity': 1
                        }, 1500, function() {
                            $(this).removeClass('text-danger');
                            $(this).addClass('text-success');
                        });
                    });
                }
            })
        });

        $('.tidaklulus').click(function() {
            let idRanking = $(this).data('idranking');
            console.log(idRanking);
            $.ajax({
                url: '/ranking/' + idRanking,
                type: 'POST',
                data: {
                    status: 0,
                    '_method': 'PUT',
                    '_token': "{{ csrf_token() }}"
                },
                success: function(result) {
                    $('#status-' + idRanking).animate({
                        'opacity': 0
                    }, 1500, function() {
                        $(this).html('tidak lulus').animate({
                            'opacity': 1
                        }, 1500, function() {
                            $(this).addClass('text-danger');
                        });
                    });
                }
            })
        });
    </script>
@endsection
