@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <h1 class="text-center mb-5">Periksa Soal Essay</h1>

        <form action="/jawaban-soal-essay" method="GET">
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <div class="card p-2 shadow-sm">
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-6 col-md-6">
                                    <small>Tahap Soal</small>
                                    <select class="form-select" aria-label="Default select example" name="tsi">
                                        <option selected disabled>Pilih Tahap Soal</option>
                                        @foreach ($tahap_soals as $tahap_soal)
                                            <option value="{{ $tahap_soal->id }}"
                                                {{ request('tsi') == $tahap_soal->id ? 'selected' : '' }}>
                                                {{ $tahap_soal->tahap_soal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 col-md-6">
                                    <small>User</small>
                                    <select class="form-select" aria-label="Default select example" name="ui">
                                        <option selected disabled>Pilih User</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}"
                                                {{ request('ui') == $user->id ? 'selected' : '' }}>{{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-primary mt-2" type="submit">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        {{-- Table start --}}
        <div class="row mt-5">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table
                            class="align-middle table table-bordered table-hover shadow-sm table-radius border border-2 text-center"
                            id="essayTable">
                            <thead class="align-middle text-bg-dark">
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Soal</th>
                                    <th scope="col">Jawaban</th>
                                    <th scope="col">File Upload</th>
                                    <th scope="col">Nilai</th>
                                    <th scope="col" class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jawabans as $jawaban)
                                    <tr>
                                        <th scope="row">{{ $jawaban->soal_essay->nomor_soal }}</th>
                                        <td class="col-table-large">
                                            <div class="card shadow-sm">
                                                <div class="card-body">
                                                    {!! $jawaban->soal_essay->soal !!}
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $jawaban->jawaban }}</td>
                                        <td>
                                            @if (substr($jawaban->jawaban_file_upload, strpos($jawaban->jawaban_file_upload, ".") + 1) == 'pdf')
                                            <a class="border-0 gambarJawaban" type="button" data-bs-toggle="modal"
                                                data-bs-target="#modal-lihat-gambar"><a target="_blank" href="/images/jawaban-essay/{{ $jawaban->jawaban_file_upload ?: 'no pict.png' }}">open PDF</a>
                                            </a>
                                            @else
                                            <button class="border-0 gambarJawaban" type="button" data-bs-toggle="modal"
                                                data-bs-target="#modal-lihat-gambar"><img height="100px" width="100px"
                                                    src="/images/jawaban-essay/{{ $jawaban->jawaban_file_upload ?: 'no pict.png' }}"
                                                    alt="tidak ada gambar">
                                            </button>
                                            @endif

                                        </td>
                                        <td class="fst-italic text-{{ $jawaban->nilai >= ($jawaban->level_soal->nilai_benar * 80) / 100 ? 'success' : ($jawaban->nilai >= ($jawaban->level_soal->nilai_benar * 50) / 100 ? 'warning' : 'danger') }} fw-bold"
                                            id="soal-{{ $jawaban->id }}">
                                            {{ $jawaban->nilai }}
                                        </td>
                                        <td colspan="2">
                                            <small class="fw-bold">nilai max :
                                                {{ $jawaban->level_soal->nilai_benar }}</small>
                                            <input type="number"
                                                class="inputNilai-{{ $jawaban->id }} form-control text-center inputNilai">
                                            <button data-id="{{ $jawaban->id }}" type="button"
                                                class="nol badge bg-danger border-0"
                                                data-bobot="{{ $jawaban->level_soal->nilai_benar }}">0</button>
                                            <button data-id="{{ $jawaban->id }}" type="button"
                                                class="max badge bg-primary border-0"
                                                data-bobot="{{ $jawaban->level_soal->nilai_benar }}">max</button>
                                            <br>
                                            <button data-id="{{ $jawaban->id }}" type="button"
                                                class="submit badge text-bg-warning border-0">submit</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- Table end --}}
    </div>

    {{-- modal lihat gambar start --}}
    <div class="modal fade" id="modal-lihat-gambar" tabindex="-1" aria-labelledby="modal-lihat-gambar" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Gambar Jawaban</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img style="width: 100%;" src="" alt="gambar-jawaban">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- modal lihat gambar end --}}

    <script>
        // Notif berhasil submit
        function berhasilSubmit() {
            const Notif = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                didOpen: (notif) => {
                    notif.addEventListener('mouseenter', Swal.stopTimer)
                    notif.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Notif.fire({
                icon: 'success',
                title: 'Berhasil memberikan nilai'
            })
        }

        // pemberian nilai start
        $('.nol').click(function() {
            let id = $(this).data('id');
            $('.inputNilai-' + id).val(0);
        });
        $('.max').click(function() {
            let bobotNilai = $(this).data('bobot');
            let id = $(this).data('id');
            $('.inputNilai-' + id).val(bobotNilai);
        });

        // Submit update data
        $('.submit').click(function() {
            let id = $(this).data('id');
            let bobotMax = $(this).parent().find('.max').attr('data-bobot');
            let inputValue = $('.inputNilai-' + id).val();
            console.log(inputValue);
            if (inputValue == '') {
                const Notif = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    didOpen: (notif) => {
                        notif.addEventListener('mouseenter', Swal.stopTimer)
                        notif.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Notif.fire({
                    icon: 'warning',
                    title: 'Tidak boleh kosong'
                })

                console.log('gagal');
                return Notif;
            }

            $.ajax({
                url: '/jawaban-soal-essay/' + id,
                method: 'PUT',
                data: {
                    '_token': "{{ csrf_token() }}",
                    nilai: parseInt(inputValue),
                    bobotMax: bobotMax
                },
                success: function(result) {
                    console.log(result);
                    let input = $('#soal-' + id);
                    berhasilSubmit();
                    input.animate({
                        'opacity': 0
                    }, 1500, function() {
                        input.text(inputValue).animate({
                            'opacity': 1
                        }, 1500, function() {
                            $(this).removeClass('text-danger');
                            $(this).removeClass('text-warning');
                            $(this).removeClass('text-success');
                            if (parseInt(inputValue) >= (bobotMax * 80 / 100)) {
                                $(this).addClass('text-success');
                            } else if (inputValue >= (bobotMax * 50 / 100)) {
                                $(this).addClass('text-warning');
                            } else {
                                $(this).addClass('text-danger');
                            }
                        });
                    });
                },
                error: function(request, error) {}
            })
        });

        $('.inputNilai').on("change keyup", function() {
            let inputValue = $(this).val();
            let bobotMax = $(this).parent().find('.max').attr('data-bobot');
            let id = $(this).parent().find('.max').attr('data-id');
            if (parseInt(inputValue) > parseInt(bobotMax)) {
                $('.inputNilai-' + id).val(bobotMax);
            } else if (parseInt(inputValue) < 0) {
                $('.inputNilai-' + id).val(0);
            }
        });
        // pemberian nilai end

        // data-tables-start
        $(document).ready(function() {
            $('#essayTable').DataTable({
                responsive: true,
                buttons: ['print', 'excel', 'pdf', 'colvis'],
                dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-5'i><'col-md-7'p>>",
                columnDefs: [{
                    className: "dt-center",
                    targets: [0, 1, 2, 3, 4, 5]
                }]
            });
        })
        // data-tables-end

        // click-open-image-start
        $('.gambarJawaban').click(function() {
            let img = $(this).find('img').attr('src');
            console.log(img);
            $('#modal-lihat-gambar').find('img').attr('src', img);
        });
        // click-open-image-end
    </script>
@endsection
