@extends('layouts.main')

@section('content')
    <div class="container py-5">
        <!-- Table Tahap Ujian Start -->
        <div class="row align-items-center">
            <div class="card shadow">
                <div class="col text-center">
                    <h1>Tahap Soal</h1>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-radius shadow-sm" id="table-tahap-soal">
                            <thead class="bg-info">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Tahap Soal</th>
                                    <th scope="col">Tanggal Ujian</th>
                                    <th scope="col">Jam Mulai</th>
                                    <th scope="col">Durasi Ujian</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tahap_soals as $tahap_soal)
                                    <tr>
                                        <th scope="row">{{ $tahap_soal->id }}</th>
                                        <td>{{ $tahap_soal->tahap_soal }}</td>
                                        <td>{{ $tahap_soal->tanggal_ujian }}</td>
                                        <td>{{ $tahap_soal->jam_mulai }}</td>
                                        <td>{{ $tahap_soal->durasi_ujian }}</td>
                                        <td>{{ $tahap_soal->status_soal }}</td>
                                        <td>
                                            <a href="tahap-soal/{{ $tahap_soal->id }}/edit"><span
                                                    class="badge bg-warning text-dark">edit</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table Tahap Ujian End -->

        <!-- Button Tambah Data Start -->
        <button class="btn btn-primary mt-3 rounded-pill {{ $errors->all() ? 'd-none' : '' }}" id="button-tambah">(+)
            Tambah Data</button>
        <!-- Button Tambah Data End -->

        <!-- Form Tambah Data Tahap Soal Start -->
        <div class="col col-md-6">
            <div class="card mt-4 shadow {{ $errors->all() ? 'show' : 'hide' }}" id="form-tambah">
                <div class="card-body">
                    <form method="POST" action="/tahap-soal">
                        @csrf
                        <div class="mb-3">
                            <label for="tahap-soal" class="form-label">Tahap Soal</label>
                            <input type="text"
                                class="form-control rounded-pill @error('tahap_soal') is-invalid @enderror" id="tahap-soal"
                                name="tahap_soal" value="{{ old('tahap_soal') }}">
                            @error('tahap_soal')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="tanggal-ujian" class="form-label">Tanggal Ujian</label>
                            <input type="date"
                                class="form-control rounded-pill @error('tanggal_ujian') is-invalid @enderror"
                                id="tanggal-ujian" name="tanggal_ujian" value="{{ old('tanggal_ujian') }}">
                            @error('tanggal_ujian')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="jam-mulai" class="form-label">Jam Mulai</label>
                            <input type="time" class="form-control rounded-pill @error('jam_mulai') is-invalid @enderror"
                                id="jam-mulai" name="jam_mulai" value="{{ old('jam_mulai') }}">
                            @error('jam_mulai')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="durasi ujian" class="form-label">Durasi Ujian</label>
                            <input type="number"
                                class="form-control rounded-pill @error('durasi_ujian') is-invalid @enderror"
                                id="durasi ujian" name="durasi_ujian" value="{{ old('durasi_ujian') }}">
                            <small class="fst-italic float-end">satuan menit</small>
                            @error('durasi_ujian')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Form Tambah Data Tahap Soal End -->
    </div>


    <script>
        // $(document).ready(function() {
        //     var table = $('#table-tahap-soal').DataTable({
        //         responsive: true,
        //         buttons: ['copy', 'csv', 'print', 'excel', 'pdf', 'colvis'],
        //         dom: "<'row'<'col-md-3'l><'col-md-6'B><'col-md-3'f>>" +
        //             "<'row'<'col-md-12'tr>>" +
        //             "<'row'<'col-md-5'i><'col-md-7'p>>",
        //         lengthMenu: [
        //             [5, 10, 15, 25, 50, -1],
        //             [5, 10, 15, 25, 50, "All"]
        //         ]
        //     });

        //     table.buttons().container()
        //         .appendTo('#table-peserta_wrapper .col-md-6:eq(0)');
        // });

        $('#button-tambah').click(function() {
            $(this).addClass('d-none');
            $('#form-tambah').removeClass('d-none');
            $('#form-tambah').toggleClass("show", 500);
        });
    </script>
@endsection
