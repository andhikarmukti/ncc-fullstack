@extends('layouts.main')

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col col-lg-6">
                <a href="/tahap-soal" class="btn btn-warning rounded-pill">Kembali</a>
                <h1 class="text-center">Edit Tahap Soal</h1>
            </div>
        </div>
        <div class="row  justify-content-center">
            <div class="col col-lg-6">
                <div class="card mt-4 shadow" id="form-tambah">
                    <div class="card-body">
                        <form method="POST" action="/tahap-soal/{{ $tahap_soal->id }}">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="tahap-soal" class="form-label">Tahap Soal</label>
                                <input type="text"
                                    class="form-control rounded-pill @error('tahap_soal') is-invalid @enderror"
                                    id="tahap-soal" name="tahap_soal"
                                    value="{{ old('tahap_soal') ?: $tahap_soal->tahap_soal }}">
                                @error('tahap_soal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="tanggal-ujian" class="form-label">Tanggal Ujian</label>
                                <input type="date"
                                    class="form-control rounded-pill @error('tanggal_ujian') is-invalid @enderror"
                                    id="tanggal-ujian" name="tanggal_ujian"
                                    value="{{ old('tanggal_ujian') ?: $tahap_soal->tanggal_ujian }}">
                                @error('tanggal_ujian')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="jam-mulai" class="form-label">Jam Mulai</label>
                                <input type="time"
                                    class="form-control rounded-pill @error('jam_mulai') is-invalid @enderror"
                                    id="jam-mulai" name="jam_mulai"
                                    value="{{ old('jam_mulai') ?: $tahap_soal->jam_mulai }}">
                            </div>
                            @error('jam_mulai')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <div class="mb-4">
                                <label for="durasi ujian" class="form-label">Durasi Ujian (<span class="text-danger fst-italic">&nbsp; satuan menit &nbsp;</span>)</label>
                                <input type="number"
                                    class="form-control rounded-pill @error('durasi_ujian') is-invalid @enderror"
                                    id="durasi ujian" name="durasi_ujian"
                                    value="{{ old('durasi_ujian') ?: $tahap_soal->durasi_ujian }}">
                                @error('durasi_ujian')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <select class="form-select rounded-pill" name="status_soal">
                                    <option value="belum dimulai" {{ $tahap_soal->status_soal == 'belum dimulai' ? 'selected' : '' }}>Belum Dimulai</option>
                                    <option value="sedang dimulai" {{ $tahap_soal->status_soal == 'sedang dimulai' ? 'selected' : '' }}>Sedang Dimulai</option>
                                    <option value="selesai" {{ $tahap_soal->status_soal == 'selesai' ? 'selected' : '' }}>Selesai</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
